Revshellgen
===========

[github.com/matesz44/scripts/blob/master/revshellgen.sh](https://github.com/matesz44/scripts/blob/master/revshellgen.sh)

*--A reverse shell payload generator--*
- Written in bash
- Yeah, I know it's bloat as hell but it's very handy
- If you have any recommendations you can open an issue or DM me on discord :D

Dependencies
------------
- [dmenu](https://tools.suckless.org/dmenu/)
- xclip (`apt install xclip`)

How does it work
----------------
![revshellgen.gif](img/revshellgen.gif)  
Open image: [revshellgen.gif](img/revshellgen.gif)

You just have to execute it and it will open some dmenu prompts where 
you can set the port and the preferred reverse shell type (bash, python, etc.).

It's not necessary to give it a port number, instead you can just press enter 
and use the program's defaults.

Defaults
--------
It has some default values:
- port:`1234`
- ipv4(*no tun0*):`10.0.0.1`
- ipv4(*tun0 connected*):**your tun0 ipv4**
- ipv6(*no tun0*):`dead:beef:2::125c`
- ipv6(*tun0 connected*):**your tun0 ipv6**
