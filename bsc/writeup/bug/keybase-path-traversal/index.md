> 2021.09.22

# Keybase allows \ (backslashes) in uploaded filenames which leads to path traversal in windows and it can be chained to rce

![path-trav-confirmed.png](path-trav-confirmed.png)

[rce-via-path-trav.mp4](rce-via-path-trav.mp4)

## Stats

```
Reported to: Keybase
Severity: High
Weakness: Path Traversal
Bounty: $2000
CVE ID: CVE-2021-34422
```

- [CVE-2021-34422](https://nvd.nist.gov/vuln/detail/CVE-2021-34422)
- [Zoom's security Bulletin](https://explore.zoom.us/en/trust/security/security-bulletin)

![mention.png](mention.png)

## Backstory

The first kick was an issue that my friend found in keybase, which was closed as informative.

The next day I decided to take a closer look at keybase because it was a 
chat application like discord but it was ***end2end encrypted***.  
Keybase is free so there was no paywall blocking me neither any paid membership crap.
Since I had experience using chat apps like discord, messenger, etc.
I didn't have to learn the basics of how to use Keybase.

Firstly I thought little issues, little leaks could also have a great impact
because of the nature of end2end encryption.

And that was the time when me and the boys started looking into Keybase's webapp and desktop clients.


My daily driver is debian and that's the reason why I preferred to look into the linux app.
In the first days we didn't find anything. We just started using keybase
and started feeling how it works and what are the differences compared to other chat applications.

Then I started reading public writeups on keybase bugs published on [hackerone's hacktivity](https://hackerone.com/keybase/hacktivity).
Some of them were privilege escalations with the installer some of them were really weird bugs,
and I also found [windows path related issues](https://hackerone.com/reports/713006).

Oh boy, my favorite was when the tester gave the file a name with a full windows path and sent it in the chat to its victim.
When the victim downloaded it the file got stored on the location where the absolute windows path pointed to. ([713006](https://hackerone.com/reports/713006))

In the next couple of days I found a bypass for one of the public issues
but the report is not public yet but I'll definitely post it as soon as it 
gets disclosed.

After getting a bounty for that report I was like damn this was great,
it's time to go to parties with frieds and other stuff like that because I had the money for it. :D

After couple of weeks, I started poking keybase again on the weekends because I
didn't have anything else to do at the moment.
I thought if there was one issue I should be able to find another.
I just started thinking about possible issues in the next week or two without even touching the application.

And when the weekend came something popped into my mind.
What if I give a file a name with a traversal in it?
I mean a windows-only traversal with backslashes like ..\\..\\..\\ because we
can name a file like this on linux (backlash \\ is allowed, unlike forwardslash /)
then keybase syncronizes it to windows and you will have a file with a traversal in its name.

I uploaded this file into a group and I traversed to an other file which was
inside of an other user's public directory.

I opened the location in windows explorer and tried to click on the file with
the traversal in in the name, but my windows 7 just errored out that the file was
inaccessible. Well fuck, it didn't work. Or maybe it did just on another windows OS?
That day I created and tried multiple traversal payloads with several apps like cmd and stuff
but none of them worked on my windows 7.
In the afternoon my friend just joined discord and ***asked why I posted a cmd icon image*** in the group.
I was like holy moly the traversal worked on his OS which was the **most recent windows 10** in this case.

We quickly tried it with the image you can see in my report (doge meme --trav--> h1 wallpaper) and it also worked.
Then we recorded the POC with the wordfiles which also worked well.

After that I tried already known windows issues like the disk corruption,
and the one where I named the file/directory to an absolute windows path like
`C:\Users\asdf\Desktop` and when the victim tried to delete the file/directory
the actual path got deleted. (For instance, the user would've deleted his own desktop
folder in this example).

However, this did not work on windows 7 but it did on XP which means microsoft
***probably*** developed windows 10 from an XP base and they completely threw
the windows vista/7 line away. This could mean you might be able to find some older
windows bugs on windows 10 which were patched in vista or 7.


## Timeline

```
Report SENT:
    April 18, 2021 16:56:07 CEST

- NTFS Disc Corruption (C:\:$i30:$bitmap) + File deletion with absolute paths:
    April 28, 2021 22:20:53 CEST
- Automatic NTFS Disc Corruption with web link icons:
    April 29, 2021 12:22:46 CEST

Report TRIAGED:
    June 7, 2021 22:31:31 CEST
Bounty:
    June 7, 2021 22:32:53 CEST

- Retest comment, (little UI issue after patch):
    June 11, 2021 16:01:02 CEST

Report RESOLVED:
    September 17, 2021 18:08:51 CEST
```

Original link: [hackerone.com/reports/1167820](https://hackerone.com/reports/1167820)
