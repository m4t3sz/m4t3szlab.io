# A place for future Bug-Bounty writeups for my disclosed reports

*--sample--*

## Stats

```
Reported to: ...
Severity: ...
Weakness: ...
Bounty: ...
CVE ID: ...
```

## Backstory

...

## Timeline

...


## Full report (exported)

Original link: [...](...)

...
