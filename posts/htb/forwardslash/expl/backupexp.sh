#!/bin/sh

usage='usage: backupexp.sh "<file u want 2 read>"'
filename=$1

fileread() {
    hashname=$(/usr/bin/backup | tail -n 1 | awk '{print $2}') # store the current time's hash
    ln -s $filename $hashname # link the given file to the hash
    /usr/bin/backup # start the backup program
}

case $# in
    "0") echo $usage ;;
    "1") fileread ;;
esac
