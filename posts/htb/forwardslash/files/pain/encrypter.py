def encrypt(key, msg):
    key = list(key)
    msg = list(msg)
    for char_key in key:
        for i in range(len(msg)):
            if i == 0:
                tmp = ord(msg[i]) + ord(char_key) + ord(msg[-1])
            else:
                tmp = ord(msg[i]) + ord(char_key) + ord(msg[i-1])

            while tmp > 255:
                tmp -= 256
            msg[i] = chr(tmp)
    return ''.join(msg)

def decrypt(key, msg):
    key = list(key)
    msg = list(msg)
    for char_key in reversed(key):
        for i in reversed(range(len(msg))):
            if i == 0:
                tmp = ord(msg[i]) - (ord(char_key) + ord(msg[-1]))
            else:
                tmp = ord(msg[i]) - (ord(char_key) + ord(msg[i-1]))
            while tmp < 0:
                tmp += 256
            msg[i] = chr(tmp)
    return ''.join(msg)


# print encrypt('REDACTED', 'REDACTED')
# print decrypt('REDACTED', encrypt('REDACTED', 'REDACTED'))

wlist = "/usr/share/wordlists/rockyou.txt"
cipher = open("ciphertext").read()
outfile = open("output.txt", "w")
with open(wlist) as fileobj:
    for line in fileobj:
        line = line.strip()
        decrypted = decrypt(line, cipher)
        outfile.write(decrypted)
outfile.close()
