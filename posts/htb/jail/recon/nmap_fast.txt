# Nmap 7.80 scan initiated Sat Apr  4 14:41:45 2020 as: nmap -oN recon/nmap_fast.txt 10.10.10.34
Nmap scan report for 10.10.10.34
Host is up (0.66s latency).
Not shown: 996 filtered ports
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
111/tcp  open  rpcbind
2049/tcp open  nfs

# Nmap done at Sat Apr  4 14:42:57 2020 -- 1 IP address (1 host up) scanned in 71.89 seconds
