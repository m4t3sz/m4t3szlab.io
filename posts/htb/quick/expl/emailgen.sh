#!/bin/sh

filename=$1

while read line; do
    echo "$line@qconsulting.uk"
    echo "$line@qconsulting.co.uk"
    echo "$line@darkwingsolutions.us"
    echo "$line@darkwingsolutions.com"
    echo "$line@wink.uk"
    echo "$line@wink.co.uk"
    echo "$line@lazycoop.cn"
    echo "$line@scoobydoo.it"
    echo "$line@penguincrop.fr"
done < $filename
