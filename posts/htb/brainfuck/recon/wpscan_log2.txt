_______________________________________________________________
        __          _______   _____                  
        \ \        / /  __ \ / ____|                 
         \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
          \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \ 
           \  /\  /  | |     ____) | (__| (_| | | | |
            \/  \/   |_|    |_____/ \___|\__,_|_| |_|

        WordPress Security Scanner by the WPScan Team 
                       Version 2.9.4
          Sponsored by Sucuri - https://sucuri.net
      @_WPScan_, @ethicalhack3r, @erwan_lr, @_FireFart_
_______________________________________________________________


[34m[i][0m It seems like you have not updated the database for some time
[34m[i][0m Last database update: 2018-06-15
[?] Do you want to update now? [Y]es  [N]o  [A]bort update, default: [N] > [32m[+][0m URL: https://brainfuck.htb/
[32m[+][0m Started: Sun Mar 29 14:18:58 2020

[32m[+][0m Interesting header: LINK: <https://brainfuck.htb/?rest_route=/>; rel="https://api.w.org/"
[32m[+][0m Interesting header: SERVER: nginx/1.10.0 (Ubuntu)
[32m[+][0m XML-RPC Interface available under: https://brainfuck.htb/xmlrpc.php   [HTTP 405]
[32m[+][0m Found an RSS Feed: https://brainfuck.htb/?feed=rss2   [HTTP 200]
[33m[!][0m Detected 1 user from RSS feed:
+-------+
| Name  |
+-------+
| admin |
+-------+

[32m[+][0m Enumerating WordPress version ...

[32m[+][0m WordPress version 4.7.3 (Released on 2017-03-06) identified from advanced fingerprinting, meta generator, links opml, stylesheets numbers
[31m[!][0m 24 vulnerabilities identified from the version number

[31m[!][0m Title: WordPress 2.3-4.8.3 - Host Header Injection in Password Reset
    Reference: https://wpvulndb.com/vulnerabilities/8807
    Reference: https://exploitbox.io/vuln/WordPress-Exploit-4-7-Unauth-Password-Reset-0day-CVE-2017-8295.html
    Reference: http://blog.dewhurstsecurity.com/2017/05/04/exploitbox-wordpress-security-advisories.html
    Reference: https://core.trac.wordpress.org/ticket/25239
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-8295

[31m[!][0m Title: WordPress 2.7.0-4.7.4 - Insufficient Redirect Validation
    Reference: https://wpvulndb.com/vulnerabilities/8815
    Reference: https://github.com/WordPress/WordPress/commit/76d77e927bb4d0f87c7262a50e28d84e01fd2b11
    Reference: https://wordpress.org/news/2017/05/wordpress-4-7-5/
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-9066
[34m[i][0m Fixed in: 4.7.5

[31m[!][0m Title: WordPress 2.5.0-4.7.4 - Post Meta Data Values Improper Handling in XML-RPC
    Reference: https://wpvulndb.com/vulnerabilities/8816
    Reference: https://wordpress.org/news/2017/05/wordpress-4-7-5/
    Reference: https://github.com/WordPress/WordPress/commit/3d95e3ae816f4d7c638f40d3e936a4be19724381
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-9062
[34m[i][0m Fixed in: 4.7.5

[31m[!][0m Title: WordPress 3.4.0-4.7.4 - XML-RPC Post Meta Data Lack of Capability Checks 
    Reference: https://wpvulndb.com/vulnerabilities/8817
    Reference: https://wordpress.org/news/2017/05/wordpress-4-7-5/
    Reference: https://github.com/WordPress/WordPress/commit/e88a48a066ab2200ce3091b131d43e2fab2460a4
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-9065
[34m[i][0m Fixed in: 4.7.5

[31m[!][0m Title: WordPress 2.5.0-4.7.4 - Filesystem Credentials Dialog CSRF
    Reference: https://wpvulndb.com/vulnerabilities/8818
    Reference: https://wordpress.org/news/2017/05/wordpress-4-7-5/
    Reference: https://github.com/WordPress/WordPress/commit/38347d7c580be4cdd8476e4bbc653d5c79ed9b67
    Reference: https://sumofpwn.nl/advisory/2016/cross_site_request_forgery_in_wordpress_connection_information.html
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-9064
[34m[i][0m Fixed in: 4.7.5

[31m[!][0m Title: WordPress 3.3-4.7.4 - Large File Upload Error XSS
    Reference: https://wpvulndb.com/vulnerabilities/8819
    Reference: https://wordpress.org/news/2017/05/wordpress-4-7-5/
    Reference: https://github.com/WordPress/WordPress/commit/8c7ea71edbbffca5d9766b7bea7c7f3722ffafa6
    Reference: https://hackerone.com/reports/203515
    Reference: https://hackerone.com/reports/203515
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-9061
[34m[i][0m Fixed in: 4.7.5

[31m[!][0m Title: WordPress 3.4.0-4.7.4 - Customizer XSS & CSRF
    Reference: https://wpvulndb.com/vulnerabilities/8820
    Reference: https://wordpress.org/news/2017/05/wordpress-4-7-5/
    Reference: https://github.com/WordPress/WordPress/commit/3d10fef22d788f29aed745b0f5ff6f6baea69af3
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-9063
[34m[i][0m Fixed in: 4.7.5

[31m[!][0m Title: WordPress 2.3.0-4.8.1 - $wpdb->prepare() potential SQL Injection
    Reference: https://wpvulndb.com/vulnerabilities/8905
    Reference: https://wordpress.org/news/2017/09/wordpress-4-8-2-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/70b21279098fc973eae803693c0705a548128e48
    Reference: https://github.com/WordPress/WordPress/commit/fc930d3daed1c3acef010d04acc2c5de93cd18ec
[34m[i][0m Fixed in: 4.7.6

[31m[!][0m Title: WordPress 2.3.0-4.7.4 - Authenticated SQL injection
    Reference: https://wpvulndb.com/vulnerabilities/8906
    Reference: https://medium.com/websec/wordpress-sqli-bbb2afcc8e94
    Reference: https://wordpress.org/news/2017/09/wordpress-4-8-2-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/70b21279098fc973eae803693c0705a548128e48
    Reference: https://wpvulndb.com/vulnerabilities/8905
[34m[i][0m Fixed in: 4.7.5

[31m[!][0m Title: WordPress 2.9.2-4.8.1 - Open Redirect
    Reference: https://wpvulndb.com/vulnerabilities/8910
    Reference: https://wordpress.org/news/2017/09/wordpress-4-8-2-security-and-maintenance-release/
    Reference: https://core.trac.wordpress.org/changeset/41398
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14725
[34m[i][0m Fixed in: 4.7.6

[31m[!][0m Title: WordPress 3.0-4.8.1 - Path Traversal in Unzipping
    Reference: https://wpvulndb.com/vulnerabilities/8911
    Reference: https://wordpress.org/news/2017/09/wordpress-4-8-2-security-and-maintenance-release/
    Reference: https://core.trac.wordpress.org/changeset/41457
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14719
[34m[i][0m Fixed in: 4.7.6

[31m[!][0m Title: WordPress 4.4-4.8.1 - Path Traversal in Customizer 
    Reference: https://wpvulndb.com/vulnerabilities/8912
    Reference: https://wordpress.org/news/2017/09/wordpress-4-8-2-security-and-maintenance-release/
    Reference: https://core.trac.wordpress.org/changeset/41397
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14722
[34m[i][0m Fixed in: 4.7.6

[31m[!][0m Title: WordPress 4.4-4.8.1 - Cross-Site Scripting (XSS) in oEmbed
    Reference: https://wpvulndb.com/vulnerabilities/8913
    Reference: https://wordpress.org/news/2017/09/wordpress-4-8-2-security-and-maintenance-release/
    Reference: https://core.trac.wordpress.org/changeset/41448
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14724
[34m[i][0m Fixed in: 4.7.6

[31m[!][0m Title: WordPress 4.2.3-4.8.1 - Authenticated Cross-Site Scripting (XSS) in Visual Editor
    Reference: https://wpvulndb.com/vulnerabilities/8914
    Reference: https://wordpress.org/news/2017/09/wordpress-4-8-2-security-and-maintenance-release/
    Reference: https://core.trac.wordpress.org/changeset/41395
    Reference: https://blog.sucuri.net/2017/09/stored-cross-site-scripting-vulnerability-in-wordpress-4-8-1.html
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14726
[34m[i][0m Fixed in: 4.7.6

[31m[!][0m Title: WordPress <= 4.8.2 - $wpdb->prepare() Weakness
    Reference: https://wpvulndb.com/vulnerabilities/8941
    Reference: https://wordpress.org/news/2017/10/wordpress-4-8-3-security-release/
    Reference: https://github.com/WordPress/WordPress/commit/a2693fd8602e3263b5925b9d799ddd577202167d
    Reference: https://twitter.com/ircmaxell/status/923662170092638208
    Reference: https://blog.ircmaxell.com/2017/10/disclosure-wordpress-wpdb-sql-injection-technical.html
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-16510
[34m[i][0m Fixed in: 4.7.7

[31m[!][0m Title: WordPress 2.8.6-4.9 - Authenticated JavaScript File Upload
    Reference: https://wpvulndb.com/vulnerabilities/8966
    Reference: https://wordpress.org/news/2017/11/wordpress-4-9-1-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/67d03a98c2cae5f41843c897f206adde299b0509
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-17092
[34m[i][0m Fixed in: 4.7.8

[31m[!][0m Title: WordPress 1.5.0-4.9 - RSS and Atom Feed Escaping
    Reference: https://wpvulndb.com/vulnerabilities/8967
    Reference: https://wordpress.org/news/2017/11/wordpress-4-9-1-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/f1de7e42df29395c3314bf85bff3d1f4f90541de
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-17094
[34m[i][0m Fixed in: 4.7.8

[31m[!][0m Title: WordPress 4.3.0-4.9 - HTML Language Attribute Escaping
    Reference: https://wpvulndb.com/vulnerabilities/8968
    Reference: https://wordpress.org/news/2017/11/wordpress-4-9-1-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/3713ac5ebc90fb2011e98dfd691420f43da6c09a
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-17093
[34m[i][0m Fixed in: 4.7.8

[31m[!][0m Title: WordPress 3.7-4.9 - 'newbloguser' Key Weak Hashing
    Reference: https://wpvulndb.com/vulnerabilities/8969
    Reference: https://wordpress.org/news/2017/11/wordpress-4-9-1-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/eaf1cfdc1fe0bdffabd8d879c591b864d833326c
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-17091
[34m[i][0m Fixed in: 4.7.8

[31m[!][0m Title: WordPress 3.7-4.9.1 - MediaElement Cross-Site Scripting (XSS)
    Reference: https://wpvulndb.com/vulnerabilities/9006
    Reference: https://github.com/WordPress/WordPress/commit/3fe9cb61ee71fcfadb5e002399296fcc1198d850
    Reference: https://wordpress.org/news/2018/01/wordpress-4-9-2-security-and-maintenance-release/
    Reference: https://core.trac.wordpress.org/ticket/42720
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-5776
[34m[i][0m Fixed in: 4.7.9

[31m[!][0m Title: WordPress <= 4.9.4 - Application Denial of Service (DoS) (unpatched)
    Reference: https://wpvulndb.com/vulnerabilities/9021
    Reference: https://baraktawily.blogspot.fr/2018/02/how-to-dos-29-of-world-wide-websites.html
    Reference: https://github.com/quitten/doser.py
    Reference: https://thehackernews.com/2018/02/wordpress-dos-exploit.html
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-6389

[31m[!][0m Title: WordPress 3.7-4.9.4 - Remove localhost Default
    Reference: https://wpvulndb.com/vulnerabilities/9053
    Reference: https://wordpress.org/news/2018/04/wordpress-4-9-5-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/804363859602d4050d9a38a21f5a65d9aec18216
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10101
[34m[i][0m Fixed in: 4.7.10

[31m[!][0m Title: WordPress 3.7-4.9.4 - Use Safe Redirect for Login
    Reference: https://wpvulndb.com/vulnerabilities/9054
    Reference: https://wordpress.org/news/2018/04/wordpress-4-9-5-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/14bc2c0a6fde0da04b47130707e01df850eedc7e
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10100
[34m[i][0m Fixed in: 4.7.10

[31m[!][0m Title: WordPress 3.7-4.9.4 - Escape Version in Generator Tag
    Reference: https://wpvulndb.com/vulnerabilities/9055
    Reference: https://wordpress.org/news/2018/04/wordpress-4-9-5-security-and-maintenance-release/
    Reference: https://github.com/WordPress/WordPress/commit/31a4369366d6b8ce30045d4c838de2412c77850d
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10102
[34m[i][0m Fixed in: 4.7.10

[32m[+][0m WordPress theme in use: proficient - v1.0.6

[32m[+][0m Name: proficient - v1.0.6
 |  Last updated: 2018-02-16T00:00:00.000Z
 |  Location: https://brainfuck.htb/wp-content/themes/proficient/
 |  Readme: https://brainfuck.htb/wp-content/themes/proficient/readme.txt
[33m[!][0m The version is out of date, the latest version is 1.1.24
 |  Style URL: https://brainfuck.htb/wp-content/themes/proficient/style.css
 |  Theme Name: Proficient
 |  Description: Proficient is a Multipurpose WordPress theme with lots of powerful features, instantly giving a p...
 |  Author: Specia
 |  Author URI: https://speciatheme.com/

[32m[+][0m Detected parent theme: specia - v2.1.1

[32m[+][0m Name: specia - v2.1.1
 |  Last updated: 2018-03-26T00:00:00.000Z
 |  Location: https://brainfuck.htb/wp-content/themes/specia/
 |  Readme: https://brainfuck.htb/wp-content/themes/specia/readme.txt
[33m[!][0m The version is out of date, the latest version is 2.2.27
 |  Style URL: https://brainfuck.htb/wp-content/themes/specia/style.css
 |  Theme Name: Specia
 |  Theme URI: https://speciatheme.com/specia-free-wordpress-theme/
 |  Description: Specia is a Multipurpose WordPress theme with lots of powerful features, instantly giving a profe...
 |  Author: Specia
 |  Author URI: https://speciatheme.com/

[32m[+][0m Enumerating plugins from passive detection ...
 | 1 plugin found:

[32m[+][0m Name: wp-support-plus-responsive-ticket-system - v7.1.3
 |  Last updated: 2018-02-22T07:11:00.000Z
 |  Location: https://brainfuck.htb/wp-content/plugins/wp-support-plus-responsive-ticket-system/
 |  Readme: https://brainfuck.htb/wp-content/plugins/wp-support-plus-responsive-ticket-system/readme.txt
[33m[!][0m The version is out of date, the latest version is 9.0.5
[33m[!][0m Directory listing is enabled: https://brainfuck.htb/wp-content/plugins/wp-support-plus-responsive-ticket-system/

[31m[!][0m Title: WP Support Plus Responsive Ticket System <= 7.1.3 – Authenticated SQL Injection
    Reference: https://wpvulndb.com/vulnerabilities/8699
    Reference: http://lenonleite.com.br/en/blog/2016/12/13/wp-support-plus-responsive-ticket-system-wordpress-plugin-sql-injection/
    Reference: https://plugins.trac.wordpress.org/changeset/1556644/wp-support-plus-responsive-ticket-system
    Reference: https://www.exploit-db.com/exploits/40939/
[34m[i][0m Fixed in: 8.0.0

[31m[!][0m Title: WP Support Plus Responsive Ticket System <= 8.0.7 - Remote Code Execution (RCE)
    Reference: https://wpvulndb.com/vulnerabilities/8949
    Reference: https://plugins.trac.wordpress.org/changeset/1763596/wp-support-plus-responsive-ticket-system
[34m[i][0m Fixed in: 8.0.8

[31m[!][0m Title: WP Support Plus Responsive Ticket System <= 9.0.2 - Multiple Authenticated SQL Injection
    Reference: https://wpvulndb.com/vulnerabilities/9041
    Reference: https://github.com/00theway/exp/blob/master/wordpress/wpsupportplus.md
    Reference: https://plugins.trac.wordpress.org/changeset/1814103/wp-support-plus-responsive-ticket-system
    Reference: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1000131
[34m[i][0m Fixed in: 9.0.3

[32m[+][0m Enumerating usernames ...
[32m[+][0m We identified the following 2 users:
    +----+---------------+---------------+
    | ID | Login         | Name          |
    +----+---------------+---------------+
    | 1  | admin         | admin         |
    | 2  | administrator | administrator |
    +----+---------------+---------------+
[33m[!][0m Default first WordPress username 'admin' is still used

[32m[+][0m Finished: Sun Mar 29 14:19:03 2020
[32m[+][0m Elapsed time: 00:00:04
[32m[+][0m Requests made: 91
[32m[+][0m Memory used: 63.953 MB
